


$(document).ready(function(){


    $('.relate-slider').slick({
      slidesToShow: 2,
      slidesToScroll: 2,
      arrows: false,
      centerMode: false,
      infinite:true,
      dots: true,
      adaptiveHeight:true,
      autoplay: true,
      autoplaySpeed: 3000,
      responsive: [
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: true,
            dots: false
          }
        }
      ]
    });

    $('.hero-relate-slider').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      centerMode: false,
      infinite:true,
      dots: true,
      adaptiveHeight:true,
      autoplay: true,
      autoplaySpeed: 3000,
      responsive: [
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: true
          }
        }
      ]
    });


    $('.gallery-slide').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      focusOnSelect: true,
      asNavFor: '.gallery-carousel'
    });

    $('.gallery-carousel').slick({
      slidesToShow: 5,
      slidesToScroll: 1,
      asNavFor: '.gallery-slide',
      dots: false,
      centerMode: true,
      adaptiveHeight: true,
      infinite:true,
      focusOnSelect: true,
      responsive: [
      {
        breakpoint: 768,
        settings: {
          centerMode: true,
          slidesToShow: 3,
          slidesToScroll: 1,
        }
      }
    ]
    });




});



$(window).on('load', function() {
      // $(".content-panel-inner").mCustomScrollbar();
      // $('.product-item').matchHeight();
});
