// Document
// https://docs.google.com/spreadsheets/d/1JWfPIYBhK05P8k26VN9tz_9VzPy99EqgCYri29PkQ7k/edit?usp=sharing


import { goLoading, goHome, goGame } from './game-manager.js' //function can call
import { MOCK_RANKING } from './const-game.js'


import FormScene from './submit-form/form-scene.js' // for testing
import { EventDispatcher, EVENT_NAME } from './helper.js' // for testing

const waitFor = delay => new Promise(resolve => setTimeout(resolve, delay));

// Callback
// Call API
const getConsentInfo = async () => {
    await waitFor(500)
    return { isConsent: false, haveUserInfo: false, userToken: 'token_ham' }
}

const sendScore = async (score, gameToken) => {
    await waitFor(500)
    console.log('sendScore', score, gameToken)
    return { success: true, rank: 15, message: null }
}

const checkUserPermissionToPlay = async () => {
    console.log('checkUserPermissionToPlay')
    await waitFor(500)
    const gameToken = "XXXXXXXXXXXXX1"
    goGame(gameToken)
    return
}

const syncDataWhenEndStage = (stage, timestamp, clicks, gameToken) => {
    console.log('syncDataWhenEndStage', stage, timestamp, clicks, gameToken)
}

const getRanks = async () => {
    await waitFor(500)
    return { 
        userInfo: {rank: 15, displayName: 'Hammmmmmmmmmmm', score: 1*1000}, 
        ranks: MOCK_RANKING
    }
}

// Call UI

const openPolicyPopupWithNoConsent = async () => {
    console.log('openPolicyPopupWithNoConsent')
    await waitFor(500)
    return true // true = checked consent, false = close
}

const openPolicyPopupFromHome = async () => {
    console.log('openPolicyPopupFromHome')
    await waitFor(500)
    return
}

const shareScore = (score) => {
    console.log('shareScore', score)
}

const shareFriends = () => {
    console.log('shareFriends')
}

// Example
const run = async () => {
    goLoading()
    const consentInfo = await getConsentInfo()
    goHome({ consentInfo
        , openPolicyPopupWithNoConsent, openPolicyPopupFromHome
        , shareFriends, shareScore
        , checkUserPermissionToPlay
        , getRanks, sendScore
        , syncDataWhenEndStage
        , isDevMode: false })    // true -> in easy mode , false -> game in real mode
        
}
run()
    
    
    
//EventDispatcher.getInstance().emit(EVENT_NAME.GO_SCORE_SCENE, 153567)
//EventDispatcher.getInstance().emit(EVENT_NAME.GO_GAME_SCENE)
//FormScene({userToken: 'xx'.userToken, gameToken: 'yy', goLeaderboard:()=>{}})
