import { SwitchBG } from './helper.js'
const IMAGES = {
    LOAD_PATH: './assets/images/game3/game',
    BG_CHEST: 'stage1-bg-chest',
    BG: 'stage1-bg',
    CHARACTER: 'stage1-character',
    COIN: 'stage1-coin-character',
    TRANSITION: 'stage1-transition-bg'

}

const Preload = (obj) => {

    obj.load.setBaseURL();
    obj.load.image(IMAGES.BG, IMAGES.LOAD_PATH+'/stage1/bg2.jpg');
    obj.load.image(IMAGES.BG_CHEST, IMAGES.LOAD_PATH+'/bg-chest.png');
    obj.load.image(IMAGES.CHARACTER, IMAGES.LOAD_PATH+'/stage1/character.png');
    obj.load.image(IMAGES.COIN, IMAGES.LOAD_PATH+'/stage1/coin.png');
    obj.load.image(IMAGES.TRANSITION, IMAGES.LOAD_PATH+'/transition-bg.png');
}
let transition, character, coin
let loopTimeline, loopTimeline2
let group

const Create = (obj) => {

    group = obj.add.group()
    let chest = obj.add.image(obj.sys.game.canvas.width/2, obj.sys.game.canvas.height/2-50, IMAGES.BG_CHEST).setOrigin(0.5,0.5);
    let bg = obj.add.image(obj.sys.game.canvas.width/2, obj.sys.game.canvas.height/2, IMAGES.BG).setOrigin(0.5,0.5);

    character = obj.add.image(obj.sys.game.canvas.width/2+70, -500, IMAGES.CHARACTER).setOrigin(0.5,0.5);
    coin = obj.add.image(obj.sys.game.canvas.width/2, obj.sys.game.canvas.height+500, IMAGES.COIN).setOrigin(0.5,1);
    transition = obj.add.image(obj.sys.game.canvas.width/2, obj.sys.game.canvas.height, IMAGES.TRANSITION).setOrigin(0.5,0);

    group.add(chest)
    group.add(bg)
    group.add(character)
    group.add(coin)
    group.add(transition)

    bg.depth = -10
    chest.depth = -11
    transition.depth = -9
    character.depth = 10
    coin.depth = 11


    const timeline = obj.tweens.timeline({
        onComplete: () => {
            timeline.destroy()
        }
    })
    SwitchBG.Stage1()
    timeline.add({
        targets: coin,
        y: obj.sys.game.canvas.height,
        duration: 400,
        delay: 1000,
        ease: Phaser.Math.Easing.Back.Out
    })

    timeline.add({
        targets: character,
        y: obj.sys.game.canvas.height-550,
        duration: 400,
        delay: 100,
        ease: Phaser.Math.Easing.Back.Out,
        onComplete: () => {
            loopTimeline = obj.tweens.timeline({
                onComplete: () => {
                    loopTimeline.destroy()
                }
            })
        
            loopTimeline.add({
                targets: character,
                y: obj.sys.game.canvas.height-650,
                duration: 900,
                angle: 5,
                ease: Phaser.Math.Easing.Quadratic.Out
            })
            loopTimeline.add({
                targets: character,
                y: obj.sys.game.canvas.height-550,
                duration: 900,
                angle: 0,
                ease: Phaser.Math.Easing.Quadratic.In
            })
            loopTimeline.loop = -1
            loopTimeline.play()

            loopTimeline2 = obj.tweens.timeline({
                onComplete: () => {
                    loopTimeline2.destroy()
                }
            })
            loopTimeline2.add({
                targets: coin,
                y: obj.sys.game.canvas.height,
                duration: 400,
                ease: Phaser.Math.Easing.Back.In
            })
            loopTimeline2.add({
                targets: coin,
                y: obj.sys.game.canvas.height+20,
                duration: 400,
                delay:1000,
                ease: Phaser.Math.Easing.Back.Out
            })
            loopTimeline2.loop = 10
            loopTimeline2.play()
        }
    })
    timeline.play()

    const loopBG = obj.tweens.timeline({
        onComplete: () => {
            loopBG.destroy()
        }
    })

}


const ChangeStage = (callback, obj) => {
    const timeline = obj.tweens.timeline({
        onComplete: () => {
            timeline.destroy()
        }
    })
    
    SwitchBG.TransitionGame()
    timeline.add({
        targets: transition,
        y: obj.sys.game.canvas.height-2500,
        duration: 900,
        ease: Phaser.Math.Easing.Back.In
    })

    timeline.play()


    const timeline2 = obj.tweens.timeline({
        onComplete: () => {
            timeline2.destroy()
        }
    })

    timeline2.add({
        targets: coin,
        y: obj.sys.game.canvas.height+500,
        duration: 400,
        ease: Phaser.Math.Easing.Back.In,
        onComplete: () => {
            if(loopTimeline){
                loopTimeline.stop()
                loopTimeline.destroy()
            }
            if(loopTimeline2){
                loopTimeline2.stop()
                loopTimeline2.destroy()
            }
        }
    })

    timeline2.add({
        targets: character,
        y: obj.sys.game.canvas.height+500,
        duration: 400,
        delay: 100,
        ease: Phaser.Math.Easing.Back.Out,
        onComplete: () => {
            clearAllSprite()
            callback()
        }
    })

    timeline2.play()

}

const clearAllSprite = () => {
    group.clear(true, true)
    //group.destroy()
}


export default { Preload, Create, ChangeStage }