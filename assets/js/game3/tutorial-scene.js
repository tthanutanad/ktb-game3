import { WEBFONT_SCRIPT } from './const-game.js'
import { EventDispatcher, EVENT_NAME, SwitchBG } from './helper.js'


const IMAGES = {
    LOAD_PATH: './assets/images/game3/tutorial',
    LOAD_BASE_PATH: './assets/images/game3',
    ASSETS:{
        BG: 'tutor-bg',
        STEP1: 'tutor-step1',
        STEP2: 'tutor-step2',
        START_BUTTON: 'tutor-start-btn',
        SKIP_BUTTON: 'tutor-skip-btn',
        NEXT_BUTTON: 'tutor-next-btn',
        HOME_BUTTON: 'tutor-home-btn',
        TRANSITION: 'leaderboard-transition-bg'
    }
}
export default class TutorialScene extends Phaser.Scene {
    constructor (config)
    {
        super(config);
    }
    init(data){
    }
    preload(){
        this.load.setBaseURL();
        this.load.image(IMAGES.ASSETS.BG, IMAGES.LOAD_BASE_PATH+'/bg.jpg');
        this.load.image(IMAGES.ASSETS.STEP1, IMAGES.LOAD_PATH+'/step-1.png');
        this.load.image(IMAGES.ASSETS.STEP2, IMAGES.LOAD_PATH+'/step-2-2.png');
        this.load.image(IMAGES.ASSETS.START_BUTTON, IMAGES.LOAD_PATH+'/start-btn.png');
        this.load.image(IMAGES.ASSETS.SKIP_BUTTON, IMAGES.LOAD_PATH+'/skip-btn.png');
        this.load.image(IMAGES.ASSETS.NEXT_BUTTON, IMAGES.LOAD_PATH+'/next-btn.png');
        this.load.image(IMAGES.ASSETS.HOME_BUTTON, IMAGES.LOAD_PATH+'/home-btn.png');
        this.load.image(IMAGES.ASSETS.TRANSITION, IMAGES.LOAD_BASE_PATH+'/transition.png');
    }
    create(){
        this.scene.bringToTop()
        let bg = this.add.image(this.sys.game.canvas.width/2, this.sys.game.canvas.height/2, IMAGES.ASSETS.BG).setOrigin(0.5,0.5);

        //Step
        //let step1 = this.add.image(this.sys.game.canvas.width/2, this.sys.game.canvas.height/2-180, IMAGES.ASSETS.STEP1).setOrigin(0.5,0.5);
        let step2 = this.add.image(this.sys.game.canvas.width/2, this.sys.game.canvas.height/2-150, IMAGES.ASSETS.STEP2).setOrigin(0.5,0.5);

        //Button
        //let nextButton = this.add.image(321, 1942, IMAGES.ASSETS.NEXT_BUTTON).setOrigin(0.5,0.5);
        //let skipButton = this.add.image(790, 1942, IMAGES.ASSETS.SKIP_BUTTON).setOrigin(0.5,0.5);
        let homeButton = this.add.image(this.sys.game.canvas.width/2, 2250, IMAGES.ASSETS.HOME_BUTTON).setOrigin(0.5,0.5);
        let startButton = this.add.image(this.sys.game.canvas.width/2, 2000, IMAGES.ASSETS.START_BUTTON).setOrigin(0.5,0.5);

        //let groupStep1 = [step1, nextButton, skipButton, homeButton]
        let groupStep2 = [step2, startButton]

        //step2.alpha = 0
        //startButton.alpha = 0

        //nextButton.setInteractive({useHandCursor: true})
        this.input.setDefaultCursor('pointer')

        // nextButton.setInteractive({useHandCursor: true}).on(Phaser.Input.Events.POINTER_UP, (pointer, localX, localY, event)=>{
        //     nextButton.disableInteractive()
        //     homeButton.disableInteractive()
        //     skipButton.disableInteractive()


        //     //Change Scene
        //     const timelineScene = this.tweens.timeline({
        //         onComplete: () => {
        //             timelineScene.destroy()
        //         }
        //     })

        //     timelineScene.add({
        //         targets: groupStep1,
        //         alpha: 0,
        //         duration: 400,
        //     })
        //     timelineScene.add({
        //         targets: groupStep2,
        //         alpha: 1,
        //         duration: 400,
        //         onComplete: () => {
        //             let loopTimeline = this.tweens.timeline({
        //                 onComplete: () => {
        //                     loopTimeline.destroy()
        //                 }
        //             })
        //             loopTimeline.add({
        //                 targets: startButton,
        //                 scaleX: 1.2,
        //                 scaleY: 1.2,
        //                 duration: 900,
        //                 ease: Phaser.Math.Easing.Quadratic.In
        //             })
        //             loopTimeline.add({
        //                 targets: startButton,
        //                 scaleX: 1,
        //                 scaleY: 1,
        //                 duration: 900,
        //                 ease: Phaser.Math.Easing.Quadratic.Out
        //             })
    
        //             loopTimeline.loop = -1
        //             loopTimeline.play()
        //         }
        //     })

        //     timelineScene.play()
        // })

        let transition = this.add.image(-this.sys.game.canvas.width, this.sys.game.canvas.height/2, IMAGES.ASSETS.TRANSITION).setOrigin(0,0.5);
        this.transition = transition
        // skipButton.setInteractive({ cursor: 'pointer' }).on(Phaser.Input.Events.POINTER_UP, (pointer, localX, localY, event)=>{
        //     // START            
        //     skipButton.disableInteractive()
        //     EventDispatcher.getInstance().emit(EVENT_NAME.TUTORIAL_START, this, (nextScene) => {
        //         this.transitionClose((obj)=>{nextScene(obj)})
        //     }, () => {
        //         try{
        //             skipButton.setInteractive()
        //         }catch(e){
        //         }
        //     });
        // })

        homeButton.setInteractive({ cursor: 'pointer' }).on(Phaser.Input.Events.POINTER_UP, (pointer, localX, localY, event)=>{
            homeButton.disableInteractive()
            // HOME
            this.transitionClose(()=>{EventDispatcher.getInstance().emit(EVENT_NAME.GO_HOME_SCENE, this);})
        })
        startButton.setInteractive({ cursor: 'pointer' }).on(Phaser.Input.Events.POINTER_UP, (pointer, localX, localY, event)=>{
            startButton.disableInteractive()
            EventDispatcher.getInstance().emit(EVENT_NAME.TUTORIAL_START, this, (nextScene) => {
                this.transitionClose((obj)=>{nextScene(obj)})
            }, () => {
                try{
                    startButton.setInteractive()
                }catch(e){
                }
            });
        })

        const timeline = this.tweens.timeline({
            onComplete: () => {
                timeline.destroy()
            }
        })
        SwitchBG.Home()
        timeline.add({
            targets: transition,
            x: this.sys.game.canvas.width*2,
            duration: 400,
            delay: 400,
        })
        timeline.play()
    }
    transitionClose(goNextScene){
        const timelineScene = this.tweens.timeline({
            onComplete: () => {
                timelineScene.destroy()
            }
        })
        SwitchBG.Transition()

        timelineScene.add({
            targets: this.transition,
            x: -this.sys.game.canvas.width,
            duration: 400,
            onComplete: ()=>{
                goNextScene(this)
            }
        })

        timelineScene.play()
    }
}