import { EventDispatcher, EVENT_NAME } from './helper.js'

const IMAGES = {
    LOAD_PATH: './assets/images/game3/thanks',
    LOAD_BASE_PATH: './assets/images/game3',
    ASSETS:{
        BG: 'thanks-bg',
        HOME_BUTTON: 'thanks-home-btn',
        TRANSITION: 'thanks-transition-bg'

    }
}

export default class ThanksScene extends Phaser.Scene {
    constructor (config)
    {
        super(config);
    }
    init(data){
    }
    preload (){
        this.load.setBaseURL();
        this.load.image(IMAGES.ASSETS.BG, IMAGES.LOAD_PATH+'/bg.png');
        //this.load.image(IMAGES.ASSETS.HOME_BUTTON, IMAGES.LOAD_PATH+'/home-btn.png');
        this.load.image(IMAGES.ASSETS.TRANSITION, IMAGES.LOAD_BASE_PATH+'/transition.png');

    }
    create(){
        this.scene.bringToTop()
        let bg = this.add.image(this.sys.game.canvas.width/2, this.sys.game.canvas.height/2, IMAGES.ASSETS.BG).setOrigin(0.5,0.5);
        //let homeButton = this.add.image(this.sys.game.canvas.width/2, 2000, IMAGES.ASSETS.HOME_BUTTON).setOrigin(0.5,0.5);

        let transition = this.add.image(this.sys.game.canvas.width*2, this.sys.game.canvas.height/2, IMAGES.ASSETS.TRANSITION).setOrigin(0,0.5);
        this.transition = transition
        // homeButton.setInteractive({ cursor: 'pointer' }).on(Phaser.Input.Events.POINTER_UP, (pointer, localX, localY, event)=>{
        //     homeButton.disableInteractive()
        //     // START
        //     this.transitionClose()
        // })
    }
    transitionClose(){
        const timelineScene = this.tweens.timeline({
            onComplete: () => {
                timelineScene.destroy()
            }
        })

        timelineScene.add({
            targets: this.transition,
            x: -this.sys.game.canvas.width,
            duration: 400,
            onComplete: () => {
                EventDispatcher.getInstance().emit(EVENT_NAME.GO_HOME_SCENE, this);
            }
        })

        timelineScene.play()
    }
}