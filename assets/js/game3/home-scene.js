import { EventDispatcher, EVENT_NAME, SwitchBG } from './helper.js'


const IMAGES = {
    LOAD_PATH: './assets/images/game3/home',
    LOAD_BASE_PATH: './assets/images/game3',
    ASSETS:{
        BG: 'home-bg',
        CHEST: 'home-chest',
        CHEST_COIN: 'home-chest-coin',
        CHARACTER: 'home-character',
        FLAG: 'home-flag',
        CARD1: 'home-card1',
        CARD2: 'home-card2',
        CARD3: 'home-card3',
        CARD4: 'home-card4',
        START_BUTTON: 'home-start-button',
        LEADERBOARD_BUTTON: 'home-leaderboard-button',
        RULE_BUTTON: 'home-rule-button',
        POLICY_BUTTON: 'home-policy-button',
        SHARE_BUTTON: 'home-share-button',
        TRANSITION: 'leaderboard-transition-bg'
    }
}

export default class HomeScene extends Phaser.Scene {
    constructor (config)
    {
        super(config);
    }
    init(data){
    }
    preload(){
        
        this.load.setBaseURL();
        this.load.image(IMAGES.ASSETS.BG, IMAGES.LOAD_BASE_PATH+'/bg.jpg');
        this.load.image(IMAGES.ASSETS.CHEST,    IMAGES.LOAD_PATH+'/chest.png');
        this.load.image(IMAGES.ASSETS.CHEST_COIN,   IMAGES.LOAD_PATH+'/chest-coin.png');
        this.load.image(IMAGES.ASSETS.CHARACTER,    IMAGES.LOAD_PATH+'/character2.png');
        this.load.image(IMAGES.ASSETS.FLAG,     IMAGES.LOAD_PATH+'/flag.png');
        this.load.image(IMAGES.ASSETS.CARD1,    IMAGES.LOAD_PATH+'/card-1.png');
        this.load.image(IMAGES.ASSETS.CARD2,    IMAGES.LOAD_PATH+'/card-2.png');
        this.load.image(IMAGES.ASSETS.CARD3,    IMAGES.LOAD_PATH+'/card-3.png');
        this.load.image(IMAGES.ASSETS.CARD4,    IMAGES.LOAD_PATH+'/card-4.png');
        this.load.image(IMAGES.ASSETS.START_BUTTON,  IMAGES.LOAD_PATH+'/start-btn.png');
        this.load.image(IMAGES.ASSETS.LEADERBOARD_BUTTON,   IMAGES.LOAD_PATH+'/leaderboard-btn.png');
        this.load.image(IMAGES.ASSETS.RULE_BUTTON,  IMAGES.LOAD_PATH+'/rule-btn.png');
        this.load.image(IMAGES.ASSETS.POLICY_BUTTON,    IMAGES.LOAD_PATH+'/policy-btn.png');
        this.load.image(IMAGES.ASSETS.SHARE_BUTTON,     IMAGES.LOAD_PATH+'/share-btn.png');
        this.load.image(IMAGES.ASSETS.TRANSITION, IMAGES.LOAD_BASE_PATH+'/transition.png');
    }
    create(){
        this.scene.bringToTop()
        let bg = this.add.image(this.sys.game.canvas.width/2, this.sys.game.canvas.height/2, IMAGES.ASSETS.BG).setOrigin(0.5,0.5);
        
        //Center
        let flag = this.add.image(500, 1150, IMAGES.ASSETS.FLAG).setOrigin(0.4,1);
        let chest = this.add.image(this.sys.game.canvas.width/2, 1060, IMAGES.ASSETS.CHEST).setOrigin(0.5,0.5);
        let character = this.add.image(604, 780, IMAGES.ASSETS.CHARACTER).setOrigin(0.5,0.5);
        let chestCoin = this.add.image(610, 1093, IMAGES.ASSETS.CHEST_COIN).setOrigin(0.5,1);

        //Card
        let card1 = this.add.image(151, 1150, IMAGES.ASSETS.CARD1).setOrigin(0.5,0.5);
        let card2 = this.add.image(1010, 1100, IMAGES.ASSETS.CARD2).setOrigin(0.5,0.5);
        let card3 = this.add.image(80, 1977, IMAGES.ASSETS.CARD3).setOrigin(0.5,0.5);
        let card4 = this.add.image(1067, 1963, IMAGES.ASSETS.CARD4).setOrigin(0.5,0.5);

        //Button
        let startButton = this.add.image(565, 1441, IMAGES.ASSETS.START_BUTTON).setOrigin(0.5,0.5);
        let leaderButton = this.add.image(571, 1684, IMAGES.ASSETS.LEADERBOARD_BUTTON).setOrigin(0.5,0.5);
        let ruleButton = this.add.image(571, 1877, IMAGES.ASSETS.RULE_BUTTON).setOrigin(0.5,0.5);
        let shareButton = this.add.image(571, 2100, IMAGES.ASSETS.SHARE_BUTTON).setOrigin(0.5,0.5);
        
        let transition = this.add.image(-this.sys.game.canvas.width, this.sys.game.canvas.height/2, IMAGES.ASSETS.TRANSITION).setOrigin(0,0.5);
        this.transition = transition
        startButton.setInteractive().on(Phaser.Input.Events.POINTER_UP, (pointer, localX, localY, event)=>{
            startButton.disableInteractive()
            EventDispatcher.getInstance().emit(EVENT_NAME.HOME_START, this, (nextScene)=>{
                this.closeTransition(startButton,nextScene)
            }, () => {
                try{
                    startButton.setInteractive()
                }catch(e){
                }
            });
        })

        leaderButton.setInteractive().on(Phaser.Input.Events.POINTER_UP, (pointer, localX, localY, event)=>{
            this.closeTransition(startButton,(obj)=>{EventDispatcher.getInstance().emit(EVENT_NAME.GO_LEADERBOARD_SCENE, obj)})
        })

        ruleButton.setInteractive().on(Phaser.Input.Events.POINTER_UP, (pointer, localX, localY, event)=>{
            EventDispatcher.getInstance().emit(EVENT_NAME.HOME_RULE, this);
        })
        shareButton.setInteractive().on(Phaser.Input.Events.POINTER_UP, (pointer, localX, localY, event)=>{
            EventDispatcher.getInstance().emit(EVENT_NAME.HOME_SHARE, this);
        })

        //Open Animation
        chest.alpha = 0
        chest.scaleX = 0.5
        chest.scaleY = 0.5

        chestCoin.scaleY = 0
        chestCoin.alpha = 0

        character.x = 604
        character.y = 780
        character.scaleX = 1//0.5
        character.scaleY = 1//0.5
        character.alpha = 0

        card1.alpha = 0
        card2.alpha = 0
        card3.alpha = 0
        card4.alpha = 0

        flag.alpha = 0
        flag.angle = 50

        startButton.alpha = 0
        leaderButton.alpha = 0
        ruleButton.alpha = 0
        shareButton.alpha = 0
        const timeline = this.tweens.timeline({
            onComplete: () => {
                timeline.destroy()
            }
        })
        SwitchBG.Home()
        timeline.add({
            targets: transition,
            x: this.sys.game.canvas.width*2,
            duration: 400,
            delay: 400,
        })
        timeline.add({
            targets: chest,
            scaleX: 1.2,
            scaleY: 1.2,
            alpha: 1,
            duration: 200,
            delay: 400,
            ease: Phaser.Math.Easing.Quadratic.Out
        })
        timeline.add({
            targets: chest,
            scaleX: 1,
            scaleY: 1,
            duration: 100,
            ease: Phaser.Math.Easing.Quadratic.In
        })
        timeline.add({
            targets: chestCoin,
            alpha:1,
            scaleY: 1,
            duration: 400,
            ease: Phaser.Math.Easing.Back.Out
        })
        timeline.add({
            targets: character,
            alpha:1,
            x: 604+100,
            y: 780-100,
            scaleX: 1.1,
            scaleY: 1.1,
            alpha: 1,
            duration: 400,
            ease: Phaser.Math.Easing.Quadratic.Out
        })
        timeline.add({
            targets: character,
            x: 604,
            y: 780,
            scaleX: 1,
            scaleY: 1,
            duration: 200,
            ease: Phaser.Math.Easing.Quadratic.In,
            onComplete: () => {
                let loopTimeline = this.tweens.timeline({
                    onComplete: () => {
                        loopTimeline.destroy()
                    }
                })
                loopTimeline.add({
                    targets: character,
                    x: 604+70,
                    y: 780-70,
                    duration: 900,
                    ease: Phaser.Math.Easing.Quadratic.In
                })
                loopTimeline.add({
                    targets: character,
                    x: 604,
                    y: 780,
                    duration: 900,
                    ease: Phaser.Math.Easing.Quadratic.Out
                })
        
                loopTimeline.loop = -1
                loopTimeline.play()
            }
        })
        timeline.add({
            targets: flag,
            angle: 0,
            alpha: 1,
            duration: 200,
            ease: Phaser.Math.Easing.Quadratic.In,
            onComplete: () => {
                let loopTimeline = this.tweens.timeline({
                    onComplete: () => {
                        loopTimeline.destroy()
                    }
                })
                loopTimeline.add({
                    targets: flag,
                    angle: -5,
                    duration: 1900,
                    //ease: Phaser.Math.Easing.Quadratic.In
                })
                loopTimeline.add({
                    targets: flag,
                    angle: 0,
                    duration: 1900,
                    //ease: Phaser.Math.Easing.Quadratic.Out
                })
        
                loopTimeline.loop = -1
                loopTimeline.play()
            }
        })

        timeline.add({
            targets: [card1, card2, card3, card4],
            alpha: 1,
            duration: 400,
            ease: Phaser.Math.Easing.Quadratic.In,
            onComplete: () => {
                let loopTimeline = this.tweens.timeline({
                    onComplete: () => {
                        loopTimeline.destroy()
                    }
                })
                loopTimeline.add({
                    targets: [card1, card2],
                    scaleX: 1.2,
                    scaleY: 1.2,
                    alpha: 0.8,
                    duration: 400,
                    ease: Phaser.Math.Easing.Quadratic.In
                })
                loopTimeline.add({
                    targets: [card1, card2],
                    scaleX: 1,
                    scaleY: 1,
                    alpha: 0.8,
                    duration: 400,
                    ease: Phaser.Math.Easing.Quadratic.Out
                })
        
                loopTimeline.loop = -1
                loopTimeline.play()

                let loopTimeline2 = this.tweens.timeline({
                    onComplete: () => {
                        loopTimeline2.destroy()
                    }
                })
                loopTimeline2.add({
                    targets: [card3, card4],
                    scaleX: 1.2,
                    scaleY: 1.2,
                    alpha: 0.8,
                    duration: 400,
                    delay: 200,
                    ease: Phaser.Math.Easing.Quadratic.In
                })
                loopTimeline2.add({
                    targets: [card3, card4],
                    scaleX: 1,
                    scaleY: 1,
                    alpha: 0.8,
                    duration: 400,
                    ease: Phaser.Math.Easing.Quadratic.Out
                })
        
                loopTimeline2.loop = -1
                loopTimeline2.play()
            }
        })

        timeline.add({
            targets: [startButton, leaderButton, ruleButton, shareButton],
            alpha: 1,
            duration: 400
        })


        timeline.play()
    }
    closeTransition(button, goNextScene){
        button.disableInteractive()
        const timelineScene = this.tweens.timeline({
            onComplete: () => {
                timelineScene.destroy()
            }
        })

        timelineScene.add({
            targets: this.transition,
            x: -this.sys.game.canvas.width,
            duration: 400,
            onComplete: () => {
                SwitchBG.Transition()
                goNextScene(this)
            }
            //ease: Phaser.Math.Easing.Back.Out
        })

        timelineScene.play()
    }
}