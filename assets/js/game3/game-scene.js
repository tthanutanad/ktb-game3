import Stage1 from './stage1.js'
import Stage2 from './stage2.js'
import Stage3 from './stage3.js'
import { EventDispatcher, EVENT_NAME } from './helper.js'


const IMAGES = {
    LOAD_PATH: './assets/images/game3',
    CARD:{
        BACK: 'back-card',
    },
    ASSETS:{
        SCROLL_STAGE1: 'scroll-1',
        SCROLL_STAGE2: 'scroll-2',
        SCROLL_STAGE3: 'scroll-3',
        HEADER: 'header',
        NO_STAGE1:  'no-stage-1',
        NO_STAGE2:  'no-stage-2',
        NO_STAGE3:  'no-stage-3',
        TRANSITION: 'game-transition-bg'
    },
    MAX_DECK_CARDS: 32,
    CARD_NAME: 'front-card-'
}

const Preload = (obj) => {
    obj.load.setBaseURL();
    //stage 1
    obj.load.image(IMAGES.CARD.BACK, IMAGES.LOAD_PATH+'/game/deck/back-min.png');
    for(let i=1;i<=IMAGES.MAX_DECK_CARDS;++i){
        obj.load.image(`${IMAGES.CARD_NAME}${i}`, IMAGES.LOAD_PATH+`/game/deck/${i}-min.png`);
    }

    obj.load.image(IMAGES.ASSETS.SCROLL_STAGE1, IMAGES.LOAD_PATH+'/game/stage1/scroll.png');
    obj.load.image(IMAGES.ASSETS.SCROLL_STAGE2, IMAGES.LOAD_PATH+'/game/stage1/scroll.png');
    obj.load.image(IMAGES.ASSETS.SCROLL_STAGE3, IMAGES.LOAD_PATH+'/game/stage3/scroll.png');

    obj.load.image(IMAGES.ASSETS.NO_STAGE1, IMAGES.LOAD_PATH+'/game/stage1/stage-1.png');
    obj.load.image(IMAGES.ASSETS.NO_STAGE2, IMAGES.LOAD_PATH+'/game/stage2/stage-2.png');
    obj.load.image(IMAGES.ASSETS.NO_STAGE3, IMAGES.LOAD_PATH+'/game/stage3/stage-3.png');

    obj.load.image(IMAGES.ASSETS.HEADER, IMAGES.LOAD_PATH+'/game/header.png');
    obj.load.image(IMAGES.ASSETS.TRANSITION, IMAGES.LOAD_PATH+'/transition.png');


    Stage1.Preload(obj)
    Stage2.Preload(obj)
    Stage3.Preload(obj)
}

const Update = (obj) => {
    if(GAME_STATE.GAME_START){
        countDownEvent(obj)
    }
}

let GAME_STATE = {
    STAGE_NO: 1,
    MAX_TIMER: 2*60*1000,
    GAME_START: false,
    SCORE_TIMER: 0,
    ASSET_GROUP: null,
    TRANSITION: null,
    GAME_DATA: null,
}

const Init = (data) => {
    const { isDevMode } = data
    //const { isFirstTime, sendScore } = data
    //console.log('init', data, goToScoreboard)
    GAME_STATE.GAME_START = false
    GAME_STATE.STAGE_NO = 1
    GAME_STATE.IS_DEV_MODE = isDevMode
    
}

const Create = (obj) => {
    stage1(obj)
    createTextTimer(obj)
    Stage1.Create(obj)
    //Stage2.Create(obj)
    //Stage3.Create(obj)

    let transition = obj.add.image(-obj.sys.game.canvas.width, obj.sys.game.canvas.height/2, IMAGES.ASSETS.TRANSITION).setOrigin(0,0.5);
    transition.depth = 20
    const timeline = obj.tweens.timeline({
        onComplete: () => {
            timeline.destroy()
        }
    })
    timeline.add({
        targets: transition,
        x: obj.sys.game.canvas.width*2,
        duration: 400,
        delay: 800,
        //ease: Phaser.Math.Easing.Back.Out
    })
    timeline.play()

    GAME_STATE.TRANSITION = transition
}

const stage1 = (obj) => {
    EventDispatcher.getInstance().emit(EVENT_NAME.GAME_CHANGE_STAGE, 0, new Date(), 0);

    const numberOfCard = GAME_STATE.IS_DEV_MODE ? 2 : 8
    generageDeck(numberOfCard, 1, obj)

    GAME_STATE.ASSET_GROUP = obj.add.group()
    assetStage1(GAME_STATE.ASSET_GROUP, obj)
}

const stage2 = (obj) => {
    GAME_STATE.STAGE_NO = 2
    const numberOfCard = GAME_STATE.IS_DEV_MODE ? 2 : 12
    generageDeck(numberOfCard, 2, obj)

    //Destroy Assets
    if(GAME_STATE.ASSET_GROUP){
        GAME_STATE.ASSET_GROUP.clear(true, true)

    }

    Stage1.ChangeStage(()=>{Stage2.Create(obj)}, obj)
    GAME_STATE.ASSET_GROUP = obj.add.group()
    assetStage2(GAME_STATE.ASSET_GROUP, obj)
}

const stage3 = (obj) => {
    GAME_STATE.STAGE_NO = 3
    const numberOfCard = GAME_STATE.IS_DEV_MODE ? 2 : 16
    generageDeck(numberOfCard, 3, obj)

    //Destroy Assets
    if(GAME_STATE.ASSET_GROUP){
        GAME_STATE.ASSET_GROUP.clear(true, true)
    }
    Stage2.ChangeStage(()=>{Stage3.Create(obj)}, obj)
    GAME_STATE.ASSET_GROUP = obj.add.group()
    assetStage3(GAME_STATE.ASSET_GROUP, obj)
}

const changeStage = (GameStage, obj) => {
    EventDispatcher.getInstance().emit(EVENT_NAME.GAME_CHANGE_STAGE, GAME_STATE.STAGE_NO, new Date(), GameStage.clicks);
    if(GAME_STATE.STAGE_NO == 1){
        destroyStage(GameStage, obj)
        stage2(obj)
    }else if(GAME_STATE.STAGE_NO == 2){
        destroyStage(GameStage, obj)
        stage3(obj)
    }else if(GAME_STATE.STAGE_NO == 3){
        destroyStage(GameStage, obj)
        //stage3(obj)
        let score = GAME_STATE.MAX_TIMER - GAME_STATE.SCORE_TIMER
        endGame(score, obj)
    }
}

const destroyStage = (GameStage, obj) => {
    for(let i=0;i<GameStage.cards.length;++i){
        if(!GameStage.cards[i])continue
        GameStage.cards[i].data = null
        GameStage.cards[i].destroy()
    }
    for(let i=0;i<GameStage.objects.length;++i){
        if(!GameStage.objects[i])continue
        GameStage.objects[i].data = null
        GameStage.objects[i].destroy()
    }
}

const generageDeck = (numberOfCard, stageNo, obj) => {    
    const cardsPerRow = numberOfCard < 4 ? numberOfCard : 4
    const startX = 180
    const startY = 689.5
    const offsetX = 250
    const offsetY = 400

    let GameStage = {
        closeCard: numberOfCard,
        cardNumbers: randomCardPosition(numberOfCard, stageNo),
        cards: [],
        objects: [],
        timer: GAME_STATE.SCORE_TIMER,
        previousCard: null,
        STATUS: {
            CLOSED: 'CLOSED',
            WAIT: 'WAIT',
            OPENED: 'OPENED'
        },
        clicks: 0,
    }
    const CARD_NO_MAP = []
    for(let i=0;i<IMAGES.MAX_DECK_CARDS;++i){
        CARD_NO_MAP.push(`${IMAGES.CARD_NAME}${i+1}`)
    }


    let cardIndex = 0
    for(let i=0;i<numberOfCard/cardsPerRow;++i){
        for(let j=0;j<cardsPerRow;++j){
            let card = obj.add.image(startX+j*offsetX, startY+i*offsetY, IMAGES.CARD.BACK).setOrigin(0.5,0.5);
            card.setScale(0.85)
            // Initial Data Card
            const cardNo = GameStage.cardNumbers[cardIndex]
            card.data = { 
                isTween: false, 
                status: GameStage.STATUS.CLOSED, 
                cardNo: cardNo, 
                frontTexture: CARD_NO_MAP[cardNo],
                flip: ()=>{flipClose(card, obj)}
            }
            GameStage.cards.push(card)
            card.setInteractive().on(Phaser.Input.Events.POINTER_UP, (pointer, localX, localY, event)=>{
                // Animation
                GameStage.clicks += 1
                flip(card, GameStage, obj)//.bind(this)
                
                //console.log('ok')
            })
            cardIndex++;
        }
    }

    GAME_STATE.GAME_DATA = GameStage
}


const randomCardPosition = (numberOfCard, stageNo) => {
    if(numberOfCard==2)return [0,0]

    // Random All Card
    let loadedCard = []
    for(let i=0;i<IMAGES.MAX_DECK_CARDS;++i)loadedCard.push(i)
    loadedCard = Phaser.Utils.Array.Shuffle(loadedCard)

    // Push in stage
    const cardStarter = {
        1: loadedCard.slice(0,4),//[0,1,2,3],
        2: loadedCard.slice(4,4+6),//[4,5,6,7,8,9],
        3: loadedCard.slice(4+6,4+6+8),//[0,1,10,11,12,13,14,15]
    }
    // Random Position
    let cardNumbers = []
    for(let i=0;i<numberOfCard;++i){
        //cardNumbers.push(Math.floor(i/2)%4)
        cardNumbers.push(cardStarter[stageNo][Math.floor(i/2)])
    }
    return Phaser.Utils.Array.Shuffle(cardNumbers)
}

const flip = (card, GameStage, obj) => {
    if(!GAME_STATE.GAME_START)return
    if(card.data && card.data.isTween) return
    if(card.data.status != GameStage.STATUS.CLOSED) return

    card.depth = 2
    card.data.isTween = true


    card.data.status = GameStage.STATUS.WAIT
    

    const timeline = obj.tweens.timeline({
        onComplete: () => {
            timeline.destroy()
        }
    })


    timeline.add({
        targets: card,
        scale: 1.15,
        duration: 50
    })

    timeline.add({
        targets:card,
        scaleX: 0,
        duration: 150,
        delay: 100,
        onComplete: () => {
            if(card.texture.key == IMAGES.CARD.BACK){
                card.setTexture(card.data.frontTexture)
            }else{
                card.setTexture(IMAGES.CARD.BACK)
            }
        }
    })

    timeline.add({
        targets: card,
        scaleX: 1.3,
        duration: 150
    })

    timeline.add({
        targets: card,
        scale: 0.85,
        duration: 150,
        onComplete: () => {
            card.depth = 0
            card.data.isTween = false
            // if match
            if(GameStage.previousCard && GameStage.previousCard.data.cardNo == card.data.cardNo){
                card.data.status = GameStage.STATUS.OPENED
                GameStage.previousCard.data.status = GameStage.STATUS.OPENED
                GameStage.previousCard = null
                GameStage.closeCard -= 2
            }else if(GameStage.previousCard && GameStage.previousCard.data.cardNo != card.data.cardNo){
                card.data.status = GameStage.STATUS.CLOSED
                GameStage.previousCard.data.status = GameStage.STATUS.CLOSED
                GameStage.previousCard.data.flip()
                card.data.flip()
                GameStage.previousCard = null
            }else{
                GameStage.previousCard = card
            }
            //END GAME
            if(GameStage.closeCard <= 0){
                changeStage(GameStage, obj)
            }

        }
    })

    timeline.play()
}


const flipClose = (card, obj) => {
    const timeline = obj.tweens.timeline({
        onComplete: () => {
            timeline.destroy()
        }
    })

    timeline.add({
        targets: card,
        scale: 0.85,
        duration: 100
    })

    timeline.add({
        targets:card,
        scaleX: 0,
        duration: 100,
        delay: 0,
        onComplete: () => {
            if(card.texture.key == IMAGES.CARD.BACK){
                card.setTexture(card.data.frontTexture)
            }else{
                card.setTexture(IMAGES.CARD.BACK)
            }
        }
    })

    timeline.add({
        targets: card,
        scaleX: 0.85,
        duration: 100
    })


    timeline.play()
}


let textTimer = null
let timeEvent = null
let startTime = null

const createTextTimer = (obj) => {
    const style = { font: "96px Segoe UI", fill: "#ffffff", align: "right" };
    textTimer = obj.add.text(556, 363.5, `${formatTime(GAME_STATE.MAX_TIMER)}`, style).setOrigin(0.5,0.5);
    textTimer.depth = 1
    //textTimer.setText(`6666`)
    //textTimer.setFontFamily('Roboto')

    //timeEvent = obj.time.addEvent({ delay: 10, callback: countDownEvent, callbackScope: this, loop: true });

}


const countDownEvent = (obj) => {
    let currentTime = new Date()
    GAME_STATE.SCORE_TIMER = currentTime - startTime
    let currentScore = GAME_STATE.MAX_TIMER - GAME_STATE.SCORE_TIMER
    if(currentScore <= 0){
        if(timeEvent)timeEvent.destroy()
        endGame(0, obj)
    }
    updateTextTimer()
}

const updateTextTimer = () => {
    let currentScore = GAME_STATE.MAX_TIMER - GAME_STATE.SCORE_TIMER
    if(currentScore<0) currentScore = 0
    textTimer.setText(`${formatTime(currentScore)}`)
}

const formatTime = (seconds) => {
    if(!seconds)seconds = 0
    let intScore = Math.floor(seconds/1000)
    let decimalScore = Math.floor((seconds%1000)/10)
    decimalScore = decimalScore.toString().padStart(2,'0');
    return `${intScore}.${decimalScore}`

    // // Minutes
    // let minutes = Math.floor(seconds/(60*1000));
    // // Seconds
    // let partInSeconds = Math.floor((seconds/(1000))%60);
    // // Adds left zeros to seconds
    // partInSeconds = partInSeconds.toString().padStart(2,'0');
    // // Milli
    // let partInMill = Math.floor(seconds%(1000)/10);
    // partInMill = partInMill.toString().padStart(2,'0');
    // // Returns formated time
    // return `${minutes}:${partInSeconds}:${partInMill}`;
}


const endGame = (score, obj) => {
    if(!GAME_STATE.GAME_START)return
    let transition = GAME_STATE.TRANSITION
    GAME_STATE.GAME_START = false
    //Change Scene
    const timelineScene = obj.tweens.timeline({
        onComplete: () => {
            timelineScene.destroy()
        }
    })

    if(score==0)destroyStage(GAME_STATE.GAME_DATA, obj)

    timelineScene.add({
        targets: transition,
        x: -obj.sys.game.canvas.width,
        duration: 400,
        onComplete: async()=>{
            EventDispatcher.getInstance().emit(EVENT_NAME.GO_SCORE_SCENE, score);
        }
        //ease: Phaser.Math.Easing.Back.Out
    })

    timelineScene.play()
}


const assetStage1 = (assetGroup, obj) => {
    const header = obj.add.image(562.5, 300.5, IMAGES.ASSETS.HEADER).setOrigin(0.5,0.5);
    const scroll = obj.add.image(560, 1027.59, IMAGES.ASSETS.SCROLL_STAGE1).setOrigin(0.5,0.5);
    const label = obj.add.image(745, 205, IMAGES.ASSETS.NO_STAGE1).setOrigin(0.5,0.5);
    label.depth = 3
    scroll.depth = -1
    scroll.scaleX = 1.15
    
    assetGroup.add(scroll)
    assetGroup.add(header)
    assetGroup.add(label)

    // Delay Start Game
    let time = obj.time.addEvent({ delay: 1000+800, callback: () => {
        startTime = new Date()
        GAME_STATE.GAME_START = true
        time.destroy()
    }, callbackScope: this, loop: false });
    tweenHeader(label, header, 800, obj)

}

const assetStage2 = (assetGroup, obj) => {
    const header = obj.add.image(562.5, 300.5, IMAGES.ASSETS.HEADER).setOrigin(0.5,0.5);
    const scroll = obj.add.image(560, 1027.59, IMAGES.ASSETS.SCROLL_STAGE2).setOrigin(0.5,0.5);
    const label = obj.add.image(745, 205, IMAGES.ASSETS.NO_STAGE2).setOrigin(0.5,0.5);
    label.depth = 3
    scroll.depth = -1
    scroll.scaleX = 1.15
    
    assetGroup.add(scroll)
    assetGroup.add(header)
    assetGroup.add(label)

    tweenHeader(label, header, 0, obj)
}


const assetStage3 = (assetGroup, obj) => {
    const header = obj.add.image(562.5, 300.5, IMAGES.ASSETS.HEADER).setOrigin(0.5,0.5);
    const scroll = obj.add.image(560, 1230, IMAGES.ASSETS.SCROLL_STAGE3).setOrigin(0.5,0.5);
    const label = obj.add.image(745, 205, IMAGES.ASSETS.NO_STAGE3).setOrigin(0.5,0.5);
    scroll.depth = -1
    label.depth = 3
    scroll.scaleX = 1.15
    scroll.scaleY = 1.05
    
    assetGroup.add(scroll)
    assetGroup.add(header)
    assetGroup.add(label)

    tweenHeader(label, header, 0, obj)
}

const tweenHeader = (label, header, delay, obj) => {
    const timeline = obj.tweens.timeline({
        onComplete: () => {
            timeline.destroy()
        }
    })

    timeline.add({
        targets: label,
        scale: 10,
        x: obj.sys.game.canvas.width/2,
        y: obj.sys.game.canvas.height/2,
        duration: 0,
    })

    timeline.add({
        targets: label,
        scale: 1,
        x: 745, 
        y: 205,
        duration: 900,
        delay: delay,
        ease: Phaser.Math.Easing.Quadratic.Out
    })
    timeline.add({
        targets: [header,label],
        scale: 0.95,
        duration: 200,
        ease: Phaser.Math.Easing.Quadratic.In
    })
    timeline.add({
        targets: [header,label],
        scale: 1,
        duration: 200,
        ease: Phaser.Math.Easing.Quadratic.Out
    })

    timeline.play()
}



export default { Init, Preload, Create, Update }