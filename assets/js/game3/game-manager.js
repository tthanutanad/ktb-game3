
//const GameScene = require('./game-scene.js')
import GameScene from './game-scene.js'
import ScoreboardScene from './scoreboard.js'
import LeaderboardScene from './leaderboard.js'
import HomeScene from './home-scene.js'
import TutorialScene from './tutorial-scene.js'
import ThanksScene from './thanks-scene.js'
import FormScene from './submit-form/form-scene.js'
import { SCENE_NAME } from './const-game.js'
import { EventDispatcher, EVENT_NAME, SwitchBG } from './helper.js'

let config = {
    type: Phaser.AUTO,
    backgroundColor: '#ffffff',
    scale: {
        mode: Phaser.Scale.FIT,
        parent: 'phaser-game3',
        autoCenter: Phaser.Scale.CENTER_BOTH,
        width: 1125,
        height: 2436,
    },
    physics: {
        default: 'arcade',
        arcade: {
            gravity: { y: 200 }
        }
    },
    scene: []
};

let gameData = {
    consentInfo: null,
    gameToken: null
}

class gameScene extends Phaser.Scene {
    constructor (config)
    {
        super(config);
    }

    init (data){
        GameScene.Init(data)
    }
    preload ()
    {
        GameScene.Preload(this)
    }

    create ()
    {
        this.scene.bringToTop()
        GameScene.Create(this)
    }

    update()
    {
        GameScene.Update(this)
    }
}
// from game to scoreboard
export const goScoreboardScene = async (score) => {
    const { success, rank, message } = await gameData.sendScore(score, gameData.gameToken)
    if(!success) throw new Error(message)
    let isFirstTime = !gameData.consentInfo.haveUserInfo
    game.scene.stop(SCENE_NAME.HOME)
    game.scene.stop(SCENE_NAME.GAME)
    game.scene.start(SCENE_NAME.SCOREBOARD, { 
        isFirstTime,
        rank,
        score, 
    })
}

const goLeaderboardScene = async () => {
    game.scene.stop(SCENE_NAME.HOME)
    game.scene.stop(SCENE_NAME.SCOREBOARD)
    game.scene.stop(SCENE_NAME.HOME)

    //Loading Ranks
    const { ranks, userInfo } = await gameData.getRanks()    

    game.scene.start(SCENE_NAME.LEADERBOARD, { 
        ranking: ranks,//MOCK_RANKING,//.slice(0,1), //TO INTEGRATE
        userInfo: userInfo,//null //{rank: 100, displayName: 'Hammmmmmmmmmmm', score: 1*1000} //TO INTEGRATE
    })
}

const goHomeScene = (obj) => {
    //debugger
    game.scene.stop(SCENE_NAME.THANKS)
    game.scene.stop(SCENE_NAME.LEADERBOARD)
    game.scene.stop(SCENE_NAME.TOTURIAL)
    game.scene.stop(SCENE_NAME.THANKS)
    game.scene.start(SCENE_NAME.HOME)
}

const goGameScene = (obj) => {
    //let isFirstTime = !gameData.consentInfo.haveUserInfo
    game.scene.stop(SCENE_NAME.HOME)
    game.scene.stop(SCENE_NAME.TOTURIAL)
    game.scene.start(SCENE_NAME.GAME, { isDevMode: gameData.isDevMode})
}

const goTutorialScene = (obj) => {
    game.scene.stop(SCENE_NAME.SCOREBOARD)
    game.scene.stop(SCENE_NAME.HOME)
    game.scene.start(SCENE_NAME.TOTURIAL)
}

// Export Section

export const goHome = (data, isSkip = false) => {
    const { consentInfo, openPolicyPopupWithNoConsent, openPolicyPopupFromHome, shareFriends 
        ,checkUserPermissionToPlay, getRanks
        ,shareScore, sendScore
        ,syncDataWhenEndStage
        ,isDevMode } = data
    gameData.consentInfo = consentInfo
    gameData.openPolicyPopupWithNoConsent = openPolicyPopupWithNoConsent
    gameData.openPolicyPopupFromHome = openPolicyPopupFromHome
    gameData.shareFriends = shareFriends
    gameData.getRanks = getRanks
    gameData.checkUserPermissionToPlay = checkUserPermissionToPlay
    gameData.syncDataWhenEndStage = syncDataWhenEndStage
    gameData.sendScore = sendScore
    gameData.shareScore = shareScore
    gameData.isDevMode = isDevMode

    if(isSkip)return
    goHomeScene()
}


// new Zone

const clickStartFromHome = async (obj, goTutorialAndTransition, enableStartButton) => {
    if(!gameData.consentInfo.isConsent){
        // First Time
        game.scene.pause(SCENE_NAME.HOME)
        const isConfirm = await gameData.openPolicyPopupWithNoConsent()
        game.scene.resume(SCENE_NAME.HOME)
        if(!isConfirm){
            enableStartButton()
            return
        }
    }
    goTutorialAndTransition(goTutorialScene)
}
const clickRulesFromHome = async () => {
    game.scene.pause(SCENE_NAME.HOME)
    await gameData.openPolicyPopupFromHome()
    game.scene.resume(SCENE_NAME.HOME)
}

const clickStartFromTutorial = async (obj, goGameAndTransition, enableButton) => {
    game.scene.pause(SCENE_NAME.TOTURIAL)
    await gameData.checkUserPermissionToPlay()
    game.scene.resume(SCENE_NAME.TOTURIAL)
    enableButton()
    gameData.goGameAndTransition = goGameAndTransition
}
const gameChangeStage = (stage, timestamp, clicks) => { 
    gameData.syncDataWhenEndStage(stage, timestamp, clicks, gameData.gameToken)
}

export const goSubmitFormScene = () => {
    console.log('submit form scene')
    gameData.consentInfo.isConsent = true
    gameData.consentInfo.haveUserInfo = true
    
    FormScene({userToken: gameData.consentInfo.userToken, gameToken: gameData.gameToken, goLeaderboard: goLeaderboardScene})
    //FormScene()
    //goLeaderboardScene()
}

const goLoadingScene = () => {
    game.scene.start(SCENE_NAME.THANKS)
}
export const goLoading = () => {
    goLoadingScene()
}

export const goGame = (gameToken='') => {
    gameData.gameToken = gameToken
    SwitchBG.Transition()
    if(gameData.goGameAndTransition) gameData.goGameAndTransition(goGameScene)
    goGameScene()
}


// Scene Manager

let game = new Phaser.Game(config);
game.scene.add(SCENE_NAME.HOME, new HomeScene(game))
game.scene.add(SCENE_NAME.LEADERBOARD, new LeaderboardScene(game))
game.scene.add(SCENE_NAME.TOTURIAL, new TutorialScene(game))
game.scene.add(SCENE_NAME.THANKS, new ThanksScene(game))
game.scene.add(SCENE_NAME.GAME, new gameScene(game));
game.scene.add(SCENE_NAME.SCOREBOARD, new ScoreboardScene(game));

const eventManager = EventDispatcher.getInstance();
eventManager.on(EVENT_NAME.TEST, (obj)=>{ console.log('ok',obj)})
eventManager.on(EVENT_NAME.GO_HOME_SCENE, goHomeScene)
eventManager.on(EVENT_NAME.GO_LEADERBOARD_SCENE, goLeaderboardScene)
eventManager.on(EVENT_NAME.GO_GAME_SCENE, goGameScene)
eventManager.on(EVENT_NAME.GO_SCORE_SCENE, goScoreboardScene)
eventManager.on(EVENT_NAME.GO_SUBMIT_FORM, goSubmitFormScene)
eventManager.on(EVENT_NAME.GO_TUTORIAL_FORM, goTutorialScene)

// HOME
eventManager.on(EVENT_NAME.HOME_START, clickStartFromHome)
eventManager.on(EVENT_NAME.HOME_RULE, clickRulesFromHome)
eventManager.on(EVENT_NAME.HOME_SHARE, () => { gameData.shareFriends() })

// TUTORIAL
eventManager.on(EVENT_NAME.TUTORIAL_START, clickStartFromTutorial)

// GAME
eventManager.on(EVENT_NAME.GAME_CHANGE_STAGE, gameChangeStage)

// SCORE
eventManager.on(EVENT_NAME.SCORE_SHARE, (score) => { gameData.shareScore(score)})

// game.scene.start(SCENE_NAME.GAME, {
//     goToScoreboard
// })

//goToLeaderboard()
//goToHome()
//goTutorial()
//goThanks()
