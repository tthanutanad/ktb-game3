export const SCENE_NAME = {
    SCOREBOARD: 'Scoreboard',
    GAME: 'Game',
    LEADERBOARD: 'Leaderboard',
    HOME: 'Home',
    TOTURIAL: 'Tutorial',
    THANKS: 'Thanks'
}

export const WEBFONT_SCRIPT = 'https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js'

export const MOCK_RANKING = [
    { rank:1, displayName:'Anna Miller',        score: 110999 },
    { rank:2, displayName:'Joshua Brock',       score: 102222 },
    { rank:3, displayName:'Randy Mcgee',        score: 90447 },
    { rank:4, displayName:'Ellen Johnson',      score: 80332 },
    { rank:5, displayName:'Rachel James',       score: 70000 },
    { rank:6, displayName:'Jill Dodson',        score: 60000 },
    { rank:7, displayName:'Bill Rogers',        score: 50543 },
    { rank:8, displayName:'Krystal Hughes',     score: 40030 },
    { rank:9, displayName:'Derek Carter',       score: 30170 },
    { rank:10, displayName:'Samuel Mcpherson',  score: 20022 },
    { rank:11, displayName:'Jared Perez',       score: 10550 },
]

