import { WEBFONT_SCRIPT } from './const-game.js'
import { EventDispatcher, EVENT_NAME, SwitchBG } from './helper.js'


const IMAGES = {
    LOAD_PATH: './assets/images/game3/scoreboard',
    LOAD_BASE_PATH: './assets/images/game3',
    ASSETS:{
        BG: 'scoreboard-bg',
        HEADER: 'scoreboard-header',
        CARD: 'scoreboard-card',
        CHARACTER: 'scoreboard-character',
        SHARE_BUTTON: 'scoreboard-share-button',
        SUBMIT_BUTTON: 'scoreboard-submit-button',
        HOME_BUTTON: 'scoreboard-home-button',
        LEADER_BUTTON: 'scoreboard-leaderboard-button',
        BG_TIMER: 'scoreboard-bg-timer',
        DIALOG: 'scoreboard-dialog',
        COIN: 'scoreboard-bg-coin',
        TRANSITION: 'scoreboard-transition-bg'
    }
}


export default class ScoreboardScene extends Phaser.Scene {
    

    constructor (config)
    {
        super(config);
    }
    init(data){
        const { score, rank, isFirstTime } = data
        this.score = score
        this.isFirstTime = isFirstTime
        this.rank = rank
        if(!this.score) this.score = 0
        
    }

    preload ()
    {
        this.load.setBaseURL();
        this.load.image(IMAGES.ASSETS.BG, IMAGES.LOAD_BASE_PATH+'/bg.jpg');
        this.load.image(IMAGES.ASSETS.COIN, IMAGES.LOAD_PATH+'/coin.png');
        this.load.image(IMAGES.ASSETS.CARD, IMAGES.LOAD_PATH+'/card.png');
        this.load.image(IMAGES.ASSETS.CHARACTER, IMAGES.LOAD_PATH+'/character2.png');
        this.load.image(IMAGES.ASSETS.HEADER, IMAGES.LOAD_PATH+'/score-header.png');
        this.load.image(IMAGES.ASSETS.SHARE_BUTTON, IMAGES.LOAD_PATH+'/share-btn2.png');
        this.load.image(IMAGES.ASSETS.HOME_BUTTON, IMAGES.LOAD_PATH+'/home-btn.png');
        this.load.image(IMAGES.ASSETS.LEADER_BUTTON, IMAGES.LOAD_PATH+'/leaderboard-btn.png');
        this.load.image(IMAGES.ASSETS.SUBMIT_BUTTON, IMAGES.LOAD_PATH+'/submit-btn2.png');
        this.load.image(IMAGES.ASSETS.BG_TIMER, IMAGES.LOAD_PATH+'/score-at.png');
        this.load.image(IMAGES.ASSETS.DIALOG, IMAGES.LOAD_PATH+'/dialog.png');
        this.load.image(IMAGES.ASSETS.TRANSITION, IMAGES.LOAD_BASE_PATH+'/transition.png');
        //console.log('preload Scoreboard')

        this.load.script('webfont', WEBFONT_SCRIPT);
    }

    create ()
    {
        this.scene.bringToTop()
        //console.log('create Scoreboard')
        let bg = this.add.image(this.sys.game.canvas.width/2, this.sys.game.canvas.height/2, IMAGES.ASSETS.BG).setOrigin(0.5,0.5);
        let coin = this.add.image(this.sys.game.canvas.width/2, 200, IMAGES.ASSETS.COIN).setOrigin(0.5,0.5);
        let card = this.add.image(this.sys.game.canvas.width/2, 1191, IMAGES.ASSETS.CARD).setOrigin(0.5,0.5);

        let character = this.add.image(624, 1620, IMAGES.ASSETS.CHARACTER).setOrigin(0.5,0.5);
        let dialog = this.add.image(500, 1220, IMAGES.ASSETS.DIALOG).setOrigin(0.5,0.5);

        
        let header = this.add.image(this.sys.game.canvas.width/2, 355, IMAGES.ASSETS.HEADER).setOrigin(0.5,0.5);
        let bgTimer = this.add.image(this.sys.game.canvas.width/2, 750, IMAGES.ASSETS.BG_TIMER).setOrigin(0.5,0.5);

        const aboveButtonY = 1954
        const belowButtonY = 2234
        let leaderButton
        let shareButton = this.add.image(246, belowButtonY, IMAGES.ASSETS.SHARE_BUTTON).setOrigin(0.5,0.5);
        let homeButton = this.add.image(780, belowButtonY, IMAGES.ASSETS.HOME_BUTTON).setOrigin(0.5,0.5);

        if(this.isFirstTime){
            leaderButton = this.add.image(546, aboveButtonY, IMAGES.ASSETS.SUBMIT_BUTTON).setOrigin(0.5,0.5);
        }else{
            leaderButton = this.add.image(546, aboveButtonY, IMAGES.ASSETS.LEADER_BUTTON).setOrigin(0.5,0.5);
        }

        
        let groupText = this.add.group()
        groupText.add(bgTimer)
        this.textTimer = this.add.text(this.sys.game.canvas.width/2, 455, `${this.formatTime(this.score)}`, {}).setOrigin(0.5,0.5);
        this.textRank = this.add.text(this.sys.game.canvas.width/2, 900, `${this.rank}`, {}).setOrigin(0.5,0.5);
        groupText.add(this.textTimer)
        groupText.add(this.textRank)

        WebFont.load({
            custom: {
                families: [ 'DB Heavent' ]
            },
            active: ()=>{
                const style = { font: "200px 'DB Heavent'", fontWeight: 'bold'
                , fill: "#f92375", align: "center"
                , stroke:"#ffffff", strokeThickness: 20
                };
                const style2 = { font: "350px 'DB Heavent'", fontWeight: 'bold'
                , fill: "#29303f", align: "center"
                , stroke:"#ffffff", strokeThickness: 20
                };
                this.textTimer.setStyle(style)
                this.textRank.setStyle(style2)
            }
        })

        let transition = this.add.image(-this.sys.game.canvas.width, this.sys.game.canvas.height/2, IMAGES.ASSETS.TRANSITION).setOrigin(0,0.5);

        // Action
        shareButton.setInteractive().on(Phaser.Input.Events.POINTER_UP, (pointer, localX, localY, event)=>{
            EventDispatcher.getInstance().emit(EVENT_NAME.SCORE_SHARE, this.score);
        })

        homeButton.setInteractive().on(Phaser.Input.Events.POINTER_UP, (pointer, localX, localY, event)=>{
            homeButton.disableInteractive()
            //Change Scene
            const timelineScene = this.tweens.timeline({
                onComplete: () => {
                    timelineScene.destroy()
                }
            })

            timelineScene.add({
                targets: transition,
                x: -this.sys.game.canvas.width,
                duration: 400,
                onComplete: () => {
                    EventDispatcher.getInstance().emit(EVENT_NAME.GO_TUTORIAL_FORM);
                }
                //ease: Phaser.Math.Easing.Back.Out
            })

            timelineScene.play()
        })

        leaderButton.setInteractive().on(Phaser.Input.Events.POINTER_UP, (pointer, localX, localY, event)=>{
            leaderButton.disableInteractive()

            //Change Scene
            const timelineScene = this.tweens.timeline({
                onComplete: () => {
                    timelineScene.destroy()
                }
            })

            timelineScene.add({
                targets: transition,
                x: -this.sys.game.canvas.width,
                duration: 400,
                onComplete: () => {
                    SwitchBG.Transition()
                    if(this.isFirstTime){
                        EventDispatcher.getInstance().emit(EVENT_NAME.GO_SUBMIT_FORM, this);
                    }else{
                        EventDispatcher.getInstance().emit(EVENT_NAME.GO_LEADERBOARD_SCENE, this);
                    }
                }
                //ease: Phaser.Math.Easing.Back.Out
            })

            timelineScene.play()
        })


        // Open Scene
        const timeline = this.tweens.timeline({
            onComplete: () => {
                timeline.destroy()
            }
        })


        card.scaleX = 0.05
        card.alpha = 0
        header.scaleX = 0

        header.y = header.y+100
        header.scaleY = 0
        header.alpha = 0

        bgTimer.scaleY = 0
        bgTimer.alpha = 0

        character.scaleX = 0
        character.scaleY = 0
        character.alpha = 0
        dialog.alpha = 0

        
        shareButton.scaleX = 0
        shareButton.scaleY = 0
        shareButton.alpha = 0

        homeButton.scaleX = 0
        homeButton.scaleY = 0
        homeButton.alpha = 0

        leaderButton.scaleX = 0
        leaderButton.scaleY = 0
        leaderButton.alpha = 0


        coin.y = -400

        this.textTimer.scaleX = 0
        this.textTimer.scaleY = 0
        this.textTimer.alpha = 0

        this.textRank.scaleX = 0
        this.textRank.scaleY = 0
        this.textRank.alpha = 0
        
        SwitchBG.Home()
        timeline.add({
            targets: transition,
            x: this.sys.game.canvas.width*2,
            duration: 400,
            delay: 800,
            //ease: Phaser.Math.Easing.Back.Out
        })
    
        timeline.add({
            targets: card,
            scaleX: 1,
            alpha: 1,
            duration: 400,
            delay: 200,
            ease: Phaser.Math.Easing.Back.Out
        })

        timeline.add({
            targets: header,
            scaleX: 1.2,
            scaleY: 1.2,
            alpha: 1,
            y: header.y-100,
            duration: 200,
            ease: Phaser.Math.Easing.Quadratic.Out
        })
        timeline.add({
            targets: header,
            scaleX: 1,
            scaleY: 1,
            duration: 100,
            ease: Phaser.Math.Easing.Quadratic.In
        })
        timeline.add({
            targets: this.textTimer,
            scaleX: 1.8,
            scaleY: 1.8,
            alpha: 0.5,
            duration: 200,
            ease: Phaser.Math.Easing.Quadratic.Out
        })
        timeline.add({
            targets: this.textTimer,
            scaleX: 1,
            scaleY: 1,
            alpha: 1,
            duration: 200,
            ease: Phaser.Math.Easing.Quadratic.In
        })

        timeline.add({
            targets: bgTimer,
            scaleY: 1,
            alpha: 1,
            duration: 200,
            ease: Phaser.Math.Easing.Quadratic.Out
        })

        timeline.add({
            targets: this.textRank,
            scaleX: 1.8,
            scaleY: 1.8,
            alpha: 0.5,
            duration: 200,
            ease: Phaser.Math.Easing.Quadratic.Out
        })
        timeline.add({
            targets: this.textRank,
            scaleX: 1,
            scaleY: 1,
            alpha: 1,
            duration: 200,
            ease: Phaser.Math.Easing.Quadratic.In
        })

        timeline.add({
            targets: [character, dialog],
            scaleX: 1.5,
            scaleY: 1.5,
            alpha: 0.5,
            duration: 150,
            ease: Phaser.Math.Easing.Quadratic.Out
        })

        timeline.add({
            targets: [character, dialog],
            scaleX: 0.8,
            scaleY: 0.8,
            alpha: 1,
            duration: 150,
            ease: Phaser.Math.Easing.Quadratic.In
        })
        timeline.add({
            targets: [character, dialog],
            scaleX: 1.2,
            scaleY: 1.2,
            duration: 50,
            ease: Phaser.Math.Easing.Quadratic.Out,
            onComplete: () => {
                let loopTimeline = this.tweens.timeline({
                    onComplete: () => {
                        loopTimeline.destroy()
                    }
                })
            
                loopTimeline.add({
                    targets: character,
                    duration: 900,
                    angle: 8,
                    ease: Phaser.Math.Easing.Quadratic.Out
                })
                loopTimeline.add({
                    targets: character,
                    duration: 400,
                    angle: 0,
                    ease: Phaser.Math.Easing.Quadratic.In
                })

                loopTimeline.loop = -1
                loopTimeline.play()
            }
        })
        timeline.add({
            targets: coin,
            y: 250,
            duration: 900,
            ease: Phaser.Math.Easing.Quadratic.Out,
            onComplete: () => {
                let loopTimeline = this.tweens.timeline({
                    onComplete: () => {
                        loopTimeline.destroy()
                    }
                })
                loopTimeline.add({
                    targets: coin,
                    y: 350,
                    duration: 1400,
                    //ease: Phaser.Math.Easing.Quadratic.In
                })
                loopTimeline.add({
                    targets: coin,
                    y: 250,
                    scaleX: 1,
                    scaleY: 1,
                    duration: 1400,
                    //ease: Phaser.Math.Easing.Quadratic.Out
                })

                loopTimeline.loop = -1
                loopTimeline.play()
            }
        })

        timeline.add({
            targets: [ shareButton, homeButton ],
            scaleX: 1.5,
            scaleY: 1.5,
            alpha: 0.5,
            duration: 150,
            ease: Phaser.Math.Easing.Quadratic.Out
        })

        timeline.add({
            targets: [ shareButton, homeButton ],
            scaleX: 0.8,
            scaleY: 0.8,
            alpha: 1,
            duration: 150,
            ease: Phaser.Math.Easing.Quadratic.In
        })
        timeline.add({
            targets: [ shareButton, homeButton ],
            scaleX: 1.1,
            scaleY: 1.1,
            alpha: 1,
            duration: 100,
            ease: Phaser.Math.Easing.Quadratic.In
        })

        timeline.add({
            targets: leaderButton,
            alpha: 1,
            scaleX: 1,
            scaleY: 1,
            duration: 400,
            onComplete: () => {
                let loopTimeline = this.tweens.timeline({
                    onComplete: () => {
                        loopTimeline.destroy()
                    }
                })
                loopTimeline.add({
                    targets: leaderButton,
                    scaleX: 1.2,
                    scaleY: 1.2,
                    duration: 900,
                    ease: Phaser.Math.Easing.Quadratic.In
                })
                loopTimeline.add({
                    targets: leaderButton,
                    scaleX: 1,
                    scaleY: 1,
                    duration: 900,
                    ease: Phaser.Math.Easing.Quadratic.Out
                })

                loopTimeline.loop = -1
                loopTimeline.play()
            }
        })

        timeline.play()
    }

    update(){
        //if(this.textTimer)this.textTimer.setText(`02:00:00`)
    }

    formatTime(score){
        if(!score)score = 0
        let intScore = Math.floor(score/1000)
        let decimalScore = Math.floor((score%1000)/10)
        decimalScore = decimalScore.toString().padStart(2,'0');
        return `${intScore}.${decimalScore} คะแนน`
    }

}