let instance = null;
export class EventDispatcher extends Phaser.Events.EventEmitter {
    constructor() {
        super();       
    }
    static getInstance() {
        if (instance == null) {
            instance = new EventDispatcher();
        }
        return instance;
    }

}

export const EVENT_NAME = {
    TEST: 'test-event',
    GO_HOME_SCENE: 'go-home-scene-event',
    HOME_START: 'home-start-event',
    HOME_RULE: 'home-rule-event',
    HOME_SHARE: 'home-share-event',
    TUTORIAL_START: 'tutorial-start-event',
    GAME_CHANGE_STAGE: 'game-change-stage-event',
    SCORE_SHARE: 'score-share-event',


    GO_LEADERBOARD_SCENE: 'go-leaderboard-scene-event',
    GO_GAME_SCENE: 'go-game-scene-event',
    GO_SCORE_SCENE: 'go-score-scene-event',
    GO_SUBMIT_FORM: 'go-submit-form-event',
    GO_TUTORIAL_FORM: 'go-tutorial-scene-event',

}


export const SwitchBG = {
    Home : () => {
        $('#phaser-game3').css('background', 'url(./assets/images/game3/bg-full.jpg)')
        $('#phaser-game3').css('background-size', 'auto 100%')
        $('#phaser-game3').css('background-position', 'center top')
        $('#phaser-game3').css('background-attachment', 'fixed')
        $('#phaser-game3').css('image-rendering', '-webkit-optimize-contrast')
        $('#phaser-game3').css('background-repeat', 'no-repeat')
    },
    Stage1 : () => {
        $('#phaser-game3').css('background', 'url(./assets/images/game3/game/stage1/bg-full.jpg)')
        $('#phaser-game3').css('background-size', 'auto 100%')
        $('#phaser-game3').css('background-position', 'center top')
        $('#phaser-game3').css('background-attachment', 'fixed')
        $('#phaser-game3').css('image-rendering', '-webkit-optimize-contrast')
        $('#phaser-game3').css('background-repeat', 'no-repeat')

    },
    Stage2 : () => {
        $('#phaser-game3').css('background', 'url(./assets/images/game3/game/stage2/bg-full.jpg)')
        $('#phaser-game3').css('background-size', 'auto 100%')
        $('#phaser-game3').css('background-position', 'center top')
        $('#phaser-game3').css('background-attachment', 'fixed')
        $('#phaser-game3').css('image-rendering', '-webkit-optimize-contrast')
        $('#phaser-game3').css('background-repeat', 'no-repeat')

    },
    Stage3 : () => {
        $('#phaser-game3').css('background', 'url(./assets/images/game3/game/stage3/bg-full.jpg)')
        $('#phaser-game3').css('background-size', 'auto 100%')
        $('#phaser-game3').css('background-position', 'center top')
        $('#phaser-game3').css('background-attachment', 'fixed')
        $('#phaser-game3').css('image-rendering', '-webkit-optimize-contrast')
        $('#phaser-game3').css('background-repeat', 'no-repeat')

    },
    Transition: () => {
        $('#phaser-game3').css('background', '#ffffff')
    },
    TransitionGame: () => {
        $('#phaser-game3').css('background', '#f2e5cf')
    }
}
