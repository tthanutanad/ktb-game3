import { SwitchBG } from './helper.js'

const IMAGES = {
    LOAD_PATH: './assets/images/game3/game',
    BG_CHEST: 'stage3-bg-chest',
    BG: 'stage3-bg',
    CHARACTER: 'stage3-character',
    COIN1: 'stage3-coin1-character',
    COIN2: 'stage3-coin2-character',
    TRANSITION: 'stage3-transition-bg'

}

const Preload = (obj) => {

    obj.load.setBaseURL();
    obj.load.image(IMAGES.BG, IMAGES.LOAD_PATH+'/stage3/bg2.jpg');
    obj.load.image(IMAGES.BG_CHEST, IMAGES.LOAD_PATH+'/bg-chest.png');
    obj.load.image(IMAGES.CHARACTER, IMAGES.LOAD_PATH+'/stage3/character.png');
    obj.load.image(IMAGES.COIN1, IMAGES.LOAD_PATH+'/stage3/coin-1.png');
    obj.load.image(IMAGES.COIN2, IMAGES.LOAD_PATH+'/stage3/coin-2.png');
    obj.load.image(IMAGES.TRANSITION, IMAGES.LOAD_PATH+'/transition-bg.png');
}
let transition, character, coin, coin2
let loopTimeline, loopTimeline2, loopTimeline3
let group

const Create = (obj) => {

    group = obj.add.group()
    let chest = obj.add.image(obj.sys.game.canvas.width/2, obj.sys.game.canvas.height/2-50, IMAGES.BG_CHEST).setOrigin(0.5,0.5);
    let bg = obj.add.image(obj.sys.game.canvas.width/2, obj.sys.game.canvas.height, IMAGES.BG).setOrigin(0.5,1);

    character = obj.add.image(obj.sys.game.canvas.width/2, obj.sys.game.canvas.height+600, IMAGES.CHARACTER).setOrigin(0.5,1);
    coin = obj.add.image(-1500, obj.sys.game.canvas.height+180, IMAGES.COIN1).setOrigin(1,1);
    coin2 = obj.add.image(obj.sys.game.canvas.width+1500, obj.sys.game.canvas.height+150, IMAGES.COIN2).setOrigin(0,1);
    transition = obj.add.image(obj.sys.game.canvas.width/2, obj.sys.game.canvas.height-2500, IMAGES.TRANSITION).setOrigin(0.5,0);

    coin.scale = 10
    coin.alpha = 0
    coin2.scale = 10
    coin2.alpha = 0
    chest.alpha = 0

    group.add(chest)
    group.add(bg)
    group.add(character)
    group.add(coin)
    group.add(coin2)
    group.add(transition)

    bg.depth = -10
    chest.depth = -11
    transition.depth = -8
    character.setScale(0.5)

    character.depth = 8
    coin.depth = 9
    coin2.depth = 9

    const timeline2 = obj.tweens.timeline({
        onComplete: () => {
            timeline2.destroy()
        }
    })

    timeline2.add({
        targets: coin2,
        x: obj.sys.game.canvas.width-300,
        scale: 0.8,
        alpha: 1,
        duration: 900,
        delay: 1400,
        ease: Phaser.Math.Easing.Quadratic.Out
    })

    timeline2.play()


    const timeline = obj.tweens.timeline({
        onComplete: () => {
            timeline.destroy()
        }
    })

    timeline.add({
        targets: transition,
        y: obj.sys.game.canvas.height,
        duration: 900,
        ease: Phaser.Math.Easing.Back.In,
        onComplete: () => {
            SwitchBG.Stage3()
        }
    })

    timeline.add({
        targets: character,
        y: obj.sys.game.canvas.height,
        duration: 400,
        ease: Phaser.Math.Easing.Back.In
    })


    timeline.add({
        targets: coin,
        x: 300,
        scale: 0.8,
        alpha: 1,
        duration: 900,
        delay: 100,
        ease: Phaser.Math.Easing.Quadratic.Out,
        onComplete: () => {
            loopTimeline = obj.tweens.timeline({
                onComplete: () => {
                    loopTimeline.destroy()
                }
            })
        
            loopTimeline.add({
                targets: character,
                scaleY: 0.95-0.5,
                scaleX: 1-0.5,
                duration: 200,
                angle: 5,
                ease: Phaser.Math.Easing.Quadratic.Out
            })
            loopTimeline.add({
                targets: character,
                scaleY: 0.9-0.5,
                scaleX: 1-0.5,
                duration: 200,
                angle: 5,
                ease: Phaser.Math.Easing.Quadratic.Out
            })
            loopTimeline.add({
                targets: character,
                scaleY: 1-0.5,
                scaleX: 1-0.5,
                duration: 200,
                angle: 0,
                delay: 100,
                ease: Phaser.Math.Easing.Quadratic.In
            })
            loopTimeline.loop = -1
            loopTimeline.play()

            loopTimeline2 = obj.tweens.timeline({
                onComplete: () => {
                    loopTimeline2.destroy()
                }
            })
            loopTimeline2.add({
                targets: coin,
                y: obj.sys.game.canvas.height+180-20,
                duration: 400,
                ease: Phaser.Math.Easing.Back.Out
            })
            loopTimeline2.add({
                targets: coin,
                y: obj.sys.game.canvas.height+180,
                duration: 400,
                delay: 200,
                ease: Phaser.Math.Easing.Back.In
            })
            loopTimeline2.loop = -1
            loopTimeline2.play()

            loopTimeline3 = obj.tweens.timeline({
                onComplete: () => {
                    loopTimeline3.destroy()
                }
            })
            loopTimeline3.add({
                targets: coin2,
                y: obj.sys.game.canvas.height+150+30,
                duration: 400,
                ease: Phaser.Math.Easing.Back.In
            })
            loopTimeline3.add({
                targets: coin2,
                y: obj.sys.game.canvas.height+150,
                duration: 400,
                delay: 200,
                ease: Phaser.Math.Easing.Back.Out
            })
            loopTimeline3.loop = -1
            loopTimeline3.play()
        }
    })
    timeline.play()

    // const timeline3 = obj.tweens.timeline({
    //     onComplete: () => {
    //         timeline3.destroy()
    //     }
    // })

    // timeline3.add({
    //     targets: bg,
    //     y: obj.sys.game.canvas.height+10,
    //     x: obj.sys.game.canvas.width/2+20,
    //     scale: 1.04,
    //     duration: 400
    // })
    // timeline3.add({
    //     targets: bg,
    //     y: obj.sys.game.canvas.height,
    //     x: obj.sys.game.canvas.width/2,
    //     scale: 1,
    //     duration: 400
    // })
    // timeline3.add({
    //     targets: bg,
    //     y: obj.sys.game.canvas.height+10,
    //     x: obj.sys.game.canvas.width/2-20,
    //     scale: 1.04,
    //     duration: 400
    // })
    // timeline3.add({
    //     targets: bg,
    //     y: obj.sys.game.canvas.height,
    //     x: obj.sys.game.canvas.width/2,
    //     scale: 1,
    //     duration: 400
    // })
    // timeline3.loop = -1
    // timeline3.play()
}


const ChangeStage = (callback, obj) => {

}

const clearAllSprite = () => {
    group.clear(true, true)
    //group.destroy()
}


export default { Preload, Create, ChangeStage }