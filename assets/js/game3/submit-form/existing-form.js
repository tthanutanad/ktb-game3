const checkValidInput = (test, str) =>{
    let regex = new RegExp(test);
    let match = regex.test(str);
    // console.log(match, regex, str);
    if (str.length == 0) {
        return { case: 1, status: false };
    } else if (match == false) {
        return { case: 2, status: false };
    } else {
        return { case: 0, status: true };
    }
}

export const registerGame = ({UTK, OTTK_GAME}) => {
    return true

    // ============ STEP : 7 ============
    var json_data = {
        user_token: UTK,
        token: OTTK_GAME,
        // user_token: "TEST",
        // token: "TEST",
        product_name: $("#select_product").val(),
        display_name: encodeURIComponent($("#display_name").val()),
        first_name: $("#firstname").val(),
        last_name: $("#lastname").val(),
        telno: $("#phone").val(),
        email: $("#email").val(),
        //address: $("#address").val(),
        province: $("#province").val(),
        // district: $("#amphure").val(),
        // sub_district: $("#tombon").val(),
        // zipcode: $("#zipcode").val(),
    };

    var checklist = {
        display_name: checkValidInput(/.+/, json_data.display_name),
        first_name: checkValidInput(/[A-zก-์]{2,}/, json_data.first_name),
        last_name: checkValidInput(/[A-zก-์]{2,}/, json_data.last_name),
        telno: checkValidInput(
            /(^0[23457][\d]{7}$|^0[689][\d]{8}$)/,
            json_data.telno
        ),
        email: checkValidInput(/[^@\s]+@[^@\s]+\.[^@\s]+/, json_data.email),
        // address: checkValidInput(/.{2,}/, json_data.address),
        province: checkValidInput(/.+/, json_data.province),
        // district: checkValidInput(/.+/, json_data.district),
        // sub_district: checkValidInput(/.+/, json_data.sub_district),
        // zipcode: checkValidInput(/[\d]{5}/, json_data.zipcode),
    };

    // console.log(checklist);
    // check case
    if (checklist.display_name.status == false) {
        let html = "";
        let testcase = checklist.display_name.case;

        if (testcase == 1) {
            html = "ข้อมูลจำเป็นต้องกรอก,<br>กรุณากรอกข้อมูลให้ครบถ้วน";
        } else if (testcase == 2) {
            html =
                "รูปแบบการกรอกข้อมูลไม่ถูกต้อง,<br>กรุณากรอกด้วยตัวหนังสือภาษาไทยหรืออังกฤษ ไม่มีช่องว่าง, โปรดระวังข้อความหยาบคาย";
        }

        Swal.fire({
            title: "ข้อมูลไม่ถูกต้อง",
            html: "<u>[ ชื่อที่ใช้แสดงในอันดับ ]</u><br>" + html,
            icon: "warning",
            confirmButtonText: "กลับไปแก้ไข",
            confirmButtonColor: "#19a7e4",
            padding: "1em",
        });
        return false;
    } else if (checklist.first_name.status == false) {
        let html = "";
        let testcase = checklist.first_name.case;

        if (testcase == 1) {
            html = "ข้อมูลจำเป็นต้องกรอก,<br>กรุณากรอกข้อมูลให้ครบถ้วน";
        } else if (testcase == 2) {
            html =
                "รูปแบบการกรอกข้อมูลไม่ถูกต้อง, กรุณากรอกชื่อจริงข้อมูลด้วยภาษาไทยหรือภาษาอังกฤษ และไม่มีช่องว่างหรืออักขระพิเศษ";
        }

        Swal.fire({
            title: "กรุณาตรวจสอบข้อมูล",
            html: "<u>[ ชื่อจริง ]</u><br>" + html,
            icon: "warning",
            confirmButtonText: "กลับไปแก้ไข",
            confirmButtonColor: "#19a7e4",
            padding: "1em",
        });
        return false;
    } else if (checklist.last_name.status == false) {
        let html = "";
        let testcase = checklist.last_name.case;

        if (testcase == 1) {
            html = "ข้อมูลจำเป็นต้องกรอก,<br>กรุณากรอกข้อมูลให้ครบถ้วน";
        } else if (testcase == 2) {
            html =
                "รูปแบบการกรอกข้อมูลไม่ถูกต้อง, กรุณากรอกข้อมูลนามสกุลด้วยภาษาไทยหรือภาษาอังกฤษ และไม่มีช่องว่างหรืออักขระพิเศษ";
        }
        Swal.fire({
            title: "กรุณาตรวจสอบข้อมูล",
            html: "<u>[ นามสกุล ]</u><br>" + html,
            icon: "warning",
            confirmButtonText: "กลับไปแก้ไข",
            confirmButtonColor: "#19a7e4",
            padding: "1em",
        });
        return false;
    } else if (checklist.telno.status == false) {
        let html = "";
        let testcase = checklist.telno.case;

        if (testcase == 1) {
            html = "ข้อมูลจำเป็นต้องกรอก,<br>กรุณากรอกข้อมูลให้ครบถ้วน";
        } else if (testcase == 2) {
            html =
                "รูปแบบการกรอกข้อมูลไม่ถูกต้อง, กรุณากรอกเป็นตัวเลขทั้งหมดติดกัน ไม่มีช่องว่าง และต้องขึ้นต้นด้วย 0";
        }

        Swal.fire({
            title: "กรุณาตรวจสอบข้อมูล",
            html: "<u>[ เบอร์โทรศัพท์ ]</u><br>" + html,
            icon: "warning",
            confirmButtonText: "กลับไปแก้ไข",
            confirmButtonColor: "#19a7e4",
            padding: "1em",
        });
        return false;
    } else if (checklist.email.status == false) {
        let html = "";
        let testcase = checklist.email.case;

        if (testcase == 1) {
            html = "ข้อมูลจำเป็นต้องกรอก,<br>กรุณากรอกข้อมูลให้ครบถ้วน";
        } else if (testcase == 2) {
            html = "รูปแบบการกรอกข้อมูลไม่ถูกต้อง, กรุณากรอกอีเมลล์ให้ถูกต้อง";
        }

        Swal.fire({
            title: "กรุณาตรวจสอบข้อมูล",
            html: "<u>[ อีเมลล์ ]</u><br>" + html,
            icon: "warning",
            confirmButtonText: "กลับไปแก้ไข",
            confirmButtonColor: "#19a7e4",
            padding: "1em",
        });
        return false;
    /*} else if (checklist.address.status == false) {
        let html = "";
        let testcase = checklist.address.case;

        if (testcase == 1) {
            html = "ข้อมูลจำเป็นต้องกรอก,<br>กรุณากรอกข้อมูลให้ครบถ้วน";
        } else if (testcase == 2) {
            html =
                "รูปแบบการกรอกข้อมูลไม่ถูกต้อง, กรุณากรอกที่อยู่เลขที่บ้าน ชื่ออาคาร ซอย ถนน หรือหมู่บ้าน";
        }

        Swal.fire({
            title: "กรุณาตรวจสอบข้อมูล",
            html: "<u>[ ที่อยู่ ]</u><br>" + html,
            icon: "warning",
            confirmButtonText: "กลับไปแก้ไข",
            confirmButtonColor: "#19a7e4",
            padding: "1em",
        });
        return false;*/
    } else if (checklist.province.status == false) {
        let html = "";
        let testcase = checklist.province.case;

        if (testcase == 1) {
            html = "ข้อมูลจำเป็นต้องกรอก,<br>กรุณาเลือกจังหวัดจากในตัวเลือก";
        } else if (testcase == 2) {
            html = "รูปแบบการกรอกข้อมูลไม่ถูกต้อง";
        }

        Swal.fire({
            title: "กรุณาตรวจสอบข้อมูล",
            html: "<u>[ จังหวัด ]</u><br>" + html,
            icon: "warning",
            confirmButtonText: "กลับไปแก้ไข",
            confirmButtonColor: "#19a7e4",
            padding: "1em",
        });
        return false;
    } /*else if (checklist.district.status == false) {
        let html = "";
        let testcase = checklist.district.case;

        if (testcase == 1) {
            html =
                "ข้อมูลจำเป็นต้องกรอก,<br>กรุณาเลือก อำเภอ/เขต จากในตัวเลือก, โดยต้องทำการเลือกจังหวัดก่อน";
        } else if (testcase == 2) {
            html = "รูปแบบการกรอกข้อมูลไม่ถูกต้อง";
        }

        Swal.fire({
            title: "กรุณาตรวจสอบข้อมูล",
            html: "<u>[ อำเภอ/เขต ]</u><br>" + html,
            icon: "warning",
            confirmButtonText: "กลับไปแก้ไข",
            confirmButtonColor: "#19a7e4",
            padding: "1em",
        });
        return false;
    } else if (checklist.sub_district.status == false) {
        let html = "";
        let testcase = checklist.sub_district.case;

        if (testcase == 1) {
            html =
                "ข้อมูลจำเป็นต้องกรอก,<br>กรุณาเลือก ตำบล/แขวง จากในตัวเลือก, โดยต้องทำการเลือก จังหวัด และ อำเภอ/เขต ก่อนตามลำดับ";
        } else if (testcase == 2) {
            html = "รูปแบบการกรอกข้อมูลไม่ถูกต้อง";
        }

        Swal.fire({
            title: "กรุณาตรวจสอบข้อมูล",
            html: "<u>[ ตำบล/แขวง ]</u><br>" + html,
            icon: "warning",
            confirmButtonText: "กลับไปแก้ไข",
            confirmButtonColor: "#19a7e4",
            padding: "1em",
        });
        return false;
    } else if (checklist.zipcode.status == false) {
        let html = "";
        let testcase = checklist.zipcode.case;

        if (testcase == 1) {
            html =
                "ข้อมูลจำเป็นต้องกรอก,<br>รหัสไปรษณีย์จะกรอกอัตโนมัติ หากทำการเลือก จังหวัด, อำเภอ/เขต และ ตำบล/แขวง ตามลำดับ, สามารถแก้ไขได้";
        } else if (testcase == 2) {
            html = "รูปแบบการกรอกข้อมูลไม่ถูกต้อง";
        }

        Swal.fire({
            title: "กรุณาตรวจสอบข้อมูล",
            html: "<u>[ ตำบล/แขวง ]</u><br>" + html,
            icon: "warning",
            confirmButtonText: "กลับไปแก้ไข",
            confirmButtonColor: "#19a7e4",
            padding: "1em",
        });
        return false;
    }*/

    Swal.fire({
        title: "การยืนยัน",
        html: "ยืนยันข้อมูลที่ต้องการส่งหรือไม่?",
        icon: "info",
        confirmButtonText: "ยืนยัน",
        confirmButtonColor: "#19a7e4",
        showCancelButton: true,
        cancelButtonText: "แก้ไข",
        cancelButtonColor: "#999",
        reverseButtons: true,
        padding: "1em",
        didOpen: function() {
            // console.log("open confirm box");
            $(".swal2-confirm").attr("id", "button_confirm");
            $(".swal2-cancel").attr("id", "button_edit");
        },
    }).then(async(result) => {
        if (result.isConfirmed) {
            // console.log(json_data);
            let ct = await msg_encrypt(json_data);
            let key = ct.key;

            $.ajax({
                type: "POST",
                url: API_URL + "/game/submitQuestionnaire",
                data: JSON.stringify({
                    a: ct.a,
                    b: ct.b,
                    c: ct.c,
                }),
                dataType: "json",
                contentType: "application/json",
                success: async function(response) {
                    // console.log(response);
                    de_response = await msg_decrypt(key, response.b, response.c);
                    // console.log(de_response);
                    /* console.log(
                        "registerGame",
                        de_response.responseCode,
                        de_response.messageResponse
                    ); */
                    if (de_response.responseCode != 0) {
                        Swal.fire({
                            title: "ไม่สามารถบันทึกได้",
                            html: "กรุณาตรวจสอบความถูกต้องของข้อมูล",
                            icon: "error",
                            confirmButtonText: "ตกลง",
                            confirmButtonColor: "#19a7e4",
                            padding: "1em",
                        });
                        return false;
                    } else {
                        Swal.fire({
                            title: "บันทึกข้อมูลเรียบร้อย",
                            html: "ขอบคุณที่ร่วมทำแบบสอบถาม",
                            icon: "success",
                            confirmButtonText: "ตกลง",
                            confirmButtonColor: "#19a7e4",
                            padding: "1em",
                        }).then(() => {
                            change_step(8);
                        });
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    // console.log("Error: registerGame");
                    Swal.fire({
                        title: "ไม่สามารถบันทึกได้",
                        html: "กรุณาลองส่งใหม่อีกครั้ง",
                        icon: "error",
                        confirmButtonText: "ตกลง",
                        confirmButtonColor: "#19a7e4",
                        padding: "1em",
                    });
                    return false;
                },
            });
        } else {
            return false;
        }
    });
}