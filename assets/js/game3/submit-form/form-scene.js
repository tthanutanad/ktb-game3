import { registerGame } from './existing-form.js'
const FormScene = ({userToken, gameToken, goLeaderboard }) => {
    $('#phaser-game3').css('visibility', 'hidden')
    $('#register').css('display', 'block')
    $('#button_submit').on('click',()=>{
        const success = registerGame({UTK: userToken, OTTK_GAME: gameToken})
        // go back to leader
        goLeaderboard()
        $('#phaser-game3').css('visibility', 'visible')
        $('#register').css('display', 'none')
        $('#register').css('visibility', 'hidden')
    })
}
export default FormScene
