import { SwitchBG } from './helper.js'

const IMAGES = {
    LOAD_PATH: './assets/images/game3/game',
    BG_CHEST: 'stage2-bg-chest',
    BG: 'stage2-bg',
    CHARACTER: 'stage2-character',
    COIN: 'stage2-coin-character',
    TRANSITION: 'stage2-transition-bg'

}

const Preload = (obj) => {

    obj.load.setBaseURL();
    obj.load.image(IMAGES.BG, IMAGES.LOAD_PATH+'/stage2/bg2.jpg');
    obj.load.image(IMAGES.BG_CHEST, IMAGES.LOAD_PATH+'/bg-chest.png');
    obj.load.image(IMAGES.CHARACTER, IMAGES.LOAD_PATH+'/stage2/character.png');
    obj.load.image(IMAGES.COIN, IMAGES.LOAD_PATH+'/stage2/coin.png');
    obj.load.image(IMAGES.TRANSITION, IMAGES.LOAD_PATH+'/transition-bg.png');
}
let transition, character, coin
let loopTimeline, loopTimeline2
let group

const Create = (obj) => {

    group = obj.add.group()
    let chest = obj.add.image(obj.sys.game.canvas.width/2, obj.sys.game.canvas.height/2-45, IMAGES.BG_CHEST).setOrigin(0.5,0.5);
    let bg = obj.add.image(obj.sys.game.canvas.width/2, obj.sys.game.canvas.height, IMAGES.BG).setOrigin(0.5,1);

    character = obj.add.image(obj.sys.game.canvas.width/2-250, obj.sys.game.canvas.height+600, IMAGES.CHARACTER).setOrigin(0.5,1);
    coin = obj.add.image(obj.sys.game.canvas.width-200, obj.sys.game.canvas.height+600, IMAGES.COIN).setOrigin(0.5,1);
    transition = obj.add.image(obj.sys.game.canvas.width/2, obj.sys.game.canvas.height-2500, IMAGES.TRANSITION).setOrigin(0.5,0);

    group.add(chest)
    group.add(bg)
    group.add(character)
    group.add(coin)
    group.add(transition)

    chest.alpha = 0
    bg.depth = -11
    chest.depth = -10
    transition.depth = -9


    const timeline = obj.tweens.timeline({
        onComplete: () => {
            timeline.destroy()
        }
    })
    
    timeline.add({
        targets: transition,
        y: obj.sys.game.canvas.height,
        duration: 900,
        ease: Phaser.Math.Easing.Back.In,
        onComplete: () => {
            SwitchBG.Stage2()
        }
    })

    timeline.add({
        targets: coin,
        y: obj.sys.game.canvas.height+50,
        duration: 400,
        ease: Phaser.Math.Easing.Back.Out
    })

    timeline.add({
        targets: character,
        y: obj.sys.game.canvas.height,
        duration: 400,
        delay: 100,
        ease: Phaser.Math.Easing.Back.Out,
        onComplete: () => {
            loopTimeline = obj.tweens.timeline({
                onComplete: () => {
                    loopTimeline.destroy()
                }
            })
        
            loopTimeline.add({
                targets: character,
                scaleY: 1.1,
                scaleX: 0.9,
                duration: 400,
                angle: 5,
                ease: Phaser.Math.Easing.Bounce.Out
            })
            loopTimeline.add({
                targets: character,
                scaleY: 1,
                scaleX: 1,
                duration: 400,
                angle: 0,
                delay: 200,
                ease: Phaser.Math.Easing.Quadratic.In
            })
            loopTimeline.loop = -1
            loopTimeline.play()

            loopTimeline2 = obj.tweens.timeline({
                onComplete: () => {
                    loopTimeline2.destroy()
                }
            })
            loopTimeline2.add({
                targets: coin,
                scale: 1.05,
                duration: 400,
                ease: Phaser.Math.Easing.Back.Out
            })
            loopTimeline2.add({
                targets: coin,
                scale: 1,
                duration: 400,
                delay: 900,
                ease: Phaser.Math.Easing.Back.In
            })
            loopTimeline2.loop = -1
            loopTimeline2.play()
        }
    })
    timeline.play()

    // const timeline3 = obj.tweens.timeline({
    //     onComplete: () => {
    //         timeline3.destroy()
    //     }
    // })

    // timeline3.add({
    //     targets: bg,
    //     y: obj.sys.game.canvas.height+10,
    //     x: obj.sys.game.canvas.width/2+20,
    //     scale: 1.04,
    //     duration: 2000
    // })
    // timeline3.add({
    //     targets: bg,
    //     y: obj.sys.game.canvas.height,
    //     x: obj.sys.game.canvas.width/2,
    //     scale: 1,
    //     duration: 2000
    // })
    // timeline3.add({
    //     targets: bg,
    //     y: obj.sys.game.canvas.height+10,
    //     x: obj.sys.game.canvas.width/2-20,
    //     scale: 1.04,
    //     duration: 2000
    // })
    // timeline3.add({
    //     targets: bg,
    //     y: obj.sys.game.canvas.height,
    //     x: obj.sys.game.canvas.width/2,
    //     scale: 1,
    //     duration: 2000
    // })
    // timeline3.loop = -1
    // timeline3.play()
}


const ChangeStage = (callback, obj) => {
    const timeline = obj.tweens.timeline({
        onComplete: () => {
            timeline.destroy()
        }
    })
    SwitchBG.TransitionGame()
    timeline.add({
        targets: transition,
        y: obj.sys.game.canvas.height-2500,
        duration: 900,
        ease: Phaser.Math.Easing.Back.In
    })

    timeline.play()


    const timeline2 = obj.tweens.timeline({
        onComplete: () => {
            timeline2.destroy()
        }
    })

    timeline2.add({
        targets: coin,
        x: obj.sys.game.canvas.width+1200,
        scale: 3,
        //alpha: { value: 0, duration: 400, ease: 'Power1' },
        duration: 400,
        ease: Phaser.Math.Easing.Back.In,
        onComplete: () => {
            if(loopTimeline){
                loopTimeline.stop()
                loopTimeline.destroy()
            }
            if(loopTimeline2){
                loopTimeline2.stop()
                loopTimeline2.destroy()
            }
        }
    })

    timeline2.add({
        targets: character,
        x: -800,
        scale: 3,
        //alpha: { value: 0, duration: 400, ease: 'Power1' },
        duration: 400,
        ease: Phaser.Math.Easing.Back.In,
        onComplete: () => {
            clearAllSprite()
            callback()
        }
    })

    timeline2.play()

}

const clearAllSprite = () => {
    group.clear(true, true)
    //group.destroy()
}


export default { Preload, Create, ChangeStage }