import { WEBFONT_SCRIPT } from './const-game.js'
import { EventDispatcher, EVENT_NAME, SwitchBG } from './helper.js'


const IMAGES = {
    LOAD_PATH: './assets/images/game3/leaderboard',
    LOAD_BASE_PATH: './assets/images/game3',
    ASSETS:{
        BG: 'leaderboard-bg',
        POPUP: 'leaderboard-popup',
        HOME_BUTTON: 'leaderboard-home-button',
        RANK1: 'leaderboard-rank1',
        RANK1_ME: 'leaderboard-rank1-me',
        RANK_OTHER: 'leaderboard-rank-other',
        RANK_OTHER_ME: 'leaderboard-rank-other-me',
        TRANSITION: 'leaderboard-transition-bg'
    }
}

export default class ScoreboardScene extends Phaser.Scene {
    constructor (config)
    {
        super(config);
    }
    init(data){
        const { ranking, userInfo } = data
        this.ranking = ranking
        this.userInfo = userInfo
    }
    preload (){
        this.load.setBaseURL();
        this.load.image(IMAGES.ASSETS.BG, IMAGES.LOAD_BASE_PATH+'/bg.jpg');
        this.load.image(IMAGES.ASSETS.POPUP, IMAGES.LOAD_PATH+'/popup.png');
        this.load.image(IMAGES.ASSETS.HOME_BUTTON, IMAGES.LOAD_PATH+'/home-btn.png');
        this.load.image(IMAGES.ASSETS.RANK1, IMAGES.LOAD_PATH+'/rank-top.png');
        this.load.image(IMAGES.ASSETS.RANK1_ME, IMAGES.LOAD_PATH+'/rank-top-me.png');
        this.load.image(IMAGES.ASSETS.RANK_OTHER, IMAGES.LOAD_PATH+'/rank-others.png');
        this.load.image(IMAGES.ASSETS.RANK_OTHER_ME, IMAGES.LOAD_PATH+'/rank-others-me.png');
        this.load.image(IMAGES.ASSETS.TRANSITION, IMAGES.LOAD_BASE_PATH+'/transition.png');

        this.load.script('webfont', WEBFONT_SCRIPT);

    }
    create (){
        this.scene.bringToTop()
        
        let bg = this.add.image(this.sys.game.canvas.width/2, this.sys.game.canvas.height/2, IMAGES.ASSETS.BG).setOrigin(0.5,0.5);
        let popup = this.add.image(this.sys.game.canvas.width/2, this.sys.game.canvas.height/2, IMAGES.ASSETS.POPUP).setOrigin(0.5,0.5);

        let homeButton = this.add.image(this.sys.game.canvas.width/2, 2213, IMAGES.ASSETS.HOME_BUTTON).setOrigin(0.5,0.5);

        
        let items = this.generatePositionAndMe()
        if(items.length > 0){
            //console.log('items',items)
            for(let i=0;i<items.length;++i){
                let imageKey
                if(i==0){
                    // First Rank
                    imageKey = IMAGES.ASSETS.RANK1
                    if(items[i].me)imageKey = IMAGES.ASSETS.RANK1_ME
                }else{
                    // Other Rank
                    imageKey = IMAGES.ASSETS.RANK_OTHER
                    if(items[i].me)imageKey = IMAGES.ASSETS.RANK_OTHER_ME
                }
                items[i].slot = this.add.image(this.sys.game.canvas.width/2, items[i].y, imageKey).setOrigin(0.5,0);
                items[i].textNo = this.add.text(230, items[i].y+40, `${this.cleanRank(items[i].rank)}`, {}).setOrigin(1,0.5);
                items[i].textName = this.add.text(260, items[i].y+40, `${this.cleanName(items[i].displayName)}`, {}).setOrigin(0,0.5);
                items[i].textScore = this.add.text(this.sys.game.canvas.width-175, items[i].y+40, `${this.cleanScore(items[i].score)}`, {}).setOrigin(1,0.5);
                if(i==0){
                    items[i].textNo.alpha = 0
                    //items[i].slot.depth = 1
                }
                if(items[i].me){
                    items[i].textNo.x = 200
                    items[i].textNo.y = items[i].y+45
                    items[i].textName.x = 230
                    items[i].textName.y = items[i].y+45
                    items[i].textScore.x = this.sys.game.canvas.width-130
                    items[i].textScore.y = items[i].y+50
                    if(items[i].rank >= 1000) items[i].textNo.x = 230
                }
            }
        }
        WebFont.load({
            custom: {
                families: [ 'DB Heavent' ]
            },
            active: ()=>{
                const styleRanking = { font: "70px 'DB Heavent' bold", fontWeight: '100'
                , fill: "#00a3e4", align: "right"
                , stroke:"#00a3e4", strokeThickness: 3
                };
                const styleName = { font: "70px 'DB Heavent' bold", fontWeight: '100'
                , fill: "#172a32", align: "left"
                , stroke:"#172a32", strokeThickness: 2
                };
                const styleScore = { font: "50px 'DB Heavent' bold", fontWeight: '100'
                , fill: "#21404e", align: "right"
                , stroke:"#21404e", strokeThickness: 1
                };
                //this.textTimer.setStyle(style)
                for(let i=0;i<items.length;++i){
                    items[i].textNo.setStyle(styleRanking)
                    items[i].textName.setStyle(styleName)
                    items[i].textScore.setStyle(styleScore)
                }
            }
        })

        let transition = this.add.image(-this.sys.game.canvas.width, this.sys.game.canvas.height/2, IMAGES.ASSETS.TRANSITION).setOrigin(0,0.5);
        homeButton.setInteractive().on(Phaser.Input.Events.POINTER_UP, (pointer, localX, localY, event)=>{
            homeButton.disableInteractive()

            //Change Scene
            const timelineScene = this.tweens.timeline({
                onComplete: () => {
                    timelineScene.destroy()
                }
            })

            timelineScene.add({
                targets: transition,
                x: -this.sys.game.canvas.width,
                duration: 400,
                onComplete: () => {
                    SwitchBG.Transition()
                    EventDispatcher.getInstance().emit(EVENT_NAME.GO_HOME_SCENE, this);
                    
                }
                //ease: Phaser.Math.Easing.Back.Out
            })

            timelineScene.play()
        })

        // Animation
        let timeline = this.tweens.timeline({
            onComplete: () => {
                timeline.destroy()
            }
        })
        SwitchBG.Home()
        timeline.add({
            targets: transition,
            x: this.sys.game.canvas.width*2,
            duration: 400,
            delay: 800,
            //ease: Phaser.Math.Easing.Back.Out
        })
        timeline.play()

        let loopTimeline = this.tweens.timeline({
            onComplete: () => {
                loopTimeline.destroy()
            }
        })
        loopTimeline.add({
            targets: homeButton,
            scaleX: 1.2,
            scaleY: 1.2,
            duration: 900,
            ease: Phaser.Math.Easing.Quadratic.In
        })
        loopTimeline.add({
            targets: homeButton,
            scaleX: 1,
            scaleY: 1,
            duration: 900,
            ease: Phaser.Math.Easing.Quadratic.Out
        })

        loopTimeline.loop = -1
        loopTimeline.play()
    }
    generatePositionAndMe(){
        // generate position
        const startY = 650
        const offsetY = 105
        const offsetMeY = 130
        const numRanking = this.ranking.length

        let result
        if(this.userInfo && this.userInfo.rank<=numRanking){
            result = this.ranking.concat([])
        }else if(this.userInfo){
            result = this.ranking.concat([this.userInfo])
        }else if(this.ranking && this.ranking.length > 0){
            result = this.ranking.concat([])
        }

        if(!result || result.length == 0) return []

        result[0].y = startY
        result[0].offsetY = offsetY
        result[0].me = false
        if(this.userInfo && this.userInfo.rank==1){
            result[0].offsetY = offsetMeY
            result[0].me = true
            result[0].displayName = this.userInfo.displayName
            result[0].score = this.userInfo.score
        }

        for(let i=1;i<result.length;++i){
            result[i].offsetY = offsetY
            result[i].me = false
            if(this.userInfo && (this.userInfo.rank == i+1 || (i==result.length-1 && this.userInfo.rank > numRanking))){
                result[i].offsetY = offsetMeY
                result[i].me = true
                result[i].displayName = this.userInfo.displayName
                result[i].score = this.userInfo.score
            }
            result[i].y = result[i-1].y+result[i-1].offsetY
        }
        return result
    }
    cleanName(text){
        const limit = 12
        if(!text)return ''
        if(text.length<limit) return text
        return `${text.substring(0,limit)}`
    }
    cleanScore(score){
        if(!score)score = 0
        let intScore = Math.floor(score/1000)
        let decimalScore = Math.floor((score%1000)/10)
        decimalScore = decimalScore.toString().padStart(2,'0');
        return `${intScore}.${decimalScore} คะแนน`
    }
    cleanRank(rank){
        if(!rank || rank <= 0) return ''
        //if(rank >= 1000) return '-'
        if(rank >= 10000) return '>10K'
        return `${rank}.`
    }
}

