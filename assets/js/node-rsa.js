/**
 * Minified by jsDelivr using Terser v5.10.0.
 * Original file: /npm/node-rsa-westartrack@0.0.2/src/NodeRSA.js
 *
 * Do NOT use SRI with dynamically generated files! More information: https://www.jsdelivr.com/using-sri-with-dynamic-files
 */
/*!
 * RSA library for Node.js
 *
 * Author: rzcoder
 * License MIT
 */
var constants = require("constants"),
    rsa = require("./libs/rsa.js"),
    crypt = require("crypto"),
    ber = require("asn1").Ber,
    _ = require("./utils")._,
    utils = require("./utils"),
    schemes = require("./schemes/schemes.js"),
    formats = require("./formats/formats.js");
void 0 === constants.RSA_NO_PADDING && (constants.RSA_NO_PADDING = 3),
    (module.exports = (function() {
        var e = {
                node10: [
                    "md4",
                    "md5",
                    "ripemd160",
                    "sha1",
                    "sha224",
                    "sha256",
                    "sha384",
                    "sha512",
                ],
                node: [
                    "md4",
                    "md5",
                    "ripemd160",
                    "sha1",
                    "sha224",
                    "sha256",
                    "sha384",
                    "sha512",
                ],
                iojs: [
                    "md4",
                    "md5",
                    "ripemd160",
                    "sha1",
                    "sha224",
                    "sha256",
                    "sha384",
                    "sha512",
                ],
                browser: ["md5", "ripemd160", "sha1", "sha256", "sha512"],
            },
            t = "pkcs1",
            i = "pkcs1",
            r = {
                private: "pkcs1-private-pem",
                "private-der": "pkcs1-private-der",
                public: "pkcs8-public-pem",
                "public-der": "pkcs8-public-der",
            };

        function n(e, r, s) {
            if (!(this instanceof n)) return new n(e, r, s);
            _.isObject(r) && ((s = r), (r = void 0)),
                (this.$options = {
                    signingScheme: i,
                    signingSchemeOptions: { hash: "sha256", saltLength: null },
                    encryptionScheme: t,
                    encryptionSchemeOptions: { hash: "sha1", label: null, padding: 3 },
                    environment: utils.detectEnvironment(),
                    rsaUtils: this,
                }),
                (this.keyPair = new rsa.Key()),
                (this.$cache = {}),
                Buffer.isBuffer(e) || _.isString(e) ?
                this.importKey(e, r) :
                _.isObject(e) && this.generateKeyPair(e.b, e.e),
                this.setOptions(s);
        }
        return (
            (n.prototype.setOptions = function(r) {
                if (
                    ((r = r || {}).environment &&
                        (this.$options.environment = r.environment),
                        r.signingScheme)
                ) {
                    if (_.isString(r.signingScheme)) {
                        var n = r.signingScheme.toLowerCase().split("-");
                        1 == n.length ?
                            e.node.indexOf(n[0]) > -1 ?
                            ((this.$options.signingSchemeOptions = { hash: n[0] }),
                                (this.$options.signingScheme = i)) :
                            ((this.$options.signingScheme = n[0]),
                                (this.$options.signingSchemeOptions = { hash: null })) :
                            ((this.$options.signingSchemeOptions = { hash: n[1] }),
                                (this.$options.signingScheme = n[0]));
                    } else
                        _.isObject(r.signingScheme) &&
                        ((this.$options.signingScheme = r.signingScheme.scheme || i),
                            (this.$options.signingSchemeOptions = _.omit(
                                r.signingScheme,
                                "scheme"
                            )));
                    if (!schemes.isSignature(this.$options.signingScheme))
                        throw Error("Unsupported signing scheme");
                    if (
                        this.$options.signingSchemeOptions.hash &&
                        -1 ===
                        e[this.$options.environment].indexOf(
                            this.$options.signingSchemeOptions.hash
                        )
                    )
                        throw Error(
                            "Unsupported hashing algorithm for " +
                            this.$options.environment +
                            " environment"
                        );
                }
                if (r.encryptionScheme) {
                    if (
                        (_.isString(r.encryptionScheme) ?
                            ((this.$options.encryptionScheme =
                                    r.encryptionScheme.toLowerCase()),
                                (this.$options.encryptionSchemeOptions = {})) :
                            _.isObject(r.encryptionScheme) &&
                            ((this.$options.encryptionScheme =
                                    r.encryptionScheme.scheme || t),
                                (this.$options.encryptionSchemeOptions = _.omit(
                                    r.encryptionScheme,
                                    "scheme"
                                ))), !schemes.isEncryption(this.$options.encryptionScheme))
                    )
                        throw Error("Unsupported encryption scheme");
                    if (
                        this.$options.encryptionSchemeOptions.hash &&
                        -1 ===
                        e[this.$options.environment].indexOf(
                            this.$options.encryptionSchemeOptions.hash
                        )
                    )
                        throw Error(
                            "Unsupported hashing algorithm for " +
                            this.$options.environment +
                            " environment"
                        );
                }
                this.keyPair.setOptions(this.$options);
            }),
            (n.prototype.generateKeyPair = function(e, t) {
                if (((t = t || 65537), (e = e || 2048) % 8 != 0))
                    throw Error("Key size must be a multiple of 8.");
                return (
                    this.keyPair.generate(e, t.toString(16)), (this.$cache = {}), this
                );
            }),
            (n.prototype.importKey = function(e, t) {
                if (!e) throw Error("Empty key given");
                if (
                    (t && (t = r[t] || t), !formats.detectAndImport(this.keyPair, e, t) && void 0 === t)
                )
                    throw Error("Key format must be specified");
                return (this.$cache = {}), this;
            }),
            (n.prototype.exportKey = function(e) {
                return (
                    (e = r[(e = e || "private")] || e),
                    this.$cache[e] ||
                    (this.$cache[e] = formats.detectAndExport(this.keyPair, e)),
                    this.$cache[e]
                );
            }),
            (n.prototype.isPrivate = function() {
                return this.keyPair.isPrivate();
            }),
            (n.prototype.isPublic = function(e) {
                return this.keyPair.isPublic(e);
            }),
            (n.prototype.isEmpty = function(e) {
                return !(this.keyPair.n || this.keyPair.e || this.keyPair.d);
            }),
            (n.prototype.encrypt = function(e, t, i) {
                return this.$$encryptKey(!1, e, t, i);
            }),
            (n.prototype.decrypt = function(e, t) {
                return this.$$decryptKey(!1, e, t);
            }),
            (n.prototype.encryptPrivate = function(e, t, i) {
                return this.$$encryptKey(!0, e, t, i);
            }),
            (n.prototype.decryptPublic = function(e, t) {
                return this.$$decryptKey(!0, e, t);
            }),
            (n.prototype.$$encryptKey = function(e, t, i, r) {
                try {
                    var n = this.keyPair.encrypt(this.$getDataForEncrypt(t, r), e);
                    return "buffer" != i && i ? n.toString(i) : n;
                } catch (e) {
                    throw Error("Error during encryption. Original error: " + e);
                }
            }),
            (n.prototype.$$decryptKey = function(e, t, i) {
                try {
                    t = _.isString(t) ? Buffer.from(t, "base64") : t;
                    var r = this.keyPair.decrypt(t, e);
                    if (null === r) throw Error("Key decrypt method returns null.");
                    return this.$getDecryptedData(r, i);
                } catch (e) {
                    throw Error(
                        "Error during decryption (probably incorrect key). Original error: " +
                        e
                    );
                }
            }),
            (n.prototype.sign = function(e, t, i) {
                if (!this.isPrivate()) throw Error("This is not private key");
                var r = this.keyPair.sign(this.$getDataForEncrypt(e, i));
                return t && "buffer" != t && (r = r.toString(t)), r;
            }),
            (n.prototype.verify = function(e, t, i, r) {
                if (!this.isPublic()) throw Error("This is not public key");
                return (
                    (r = r && "buffer" != r ? r : null),
                    this.keyPair.verify(this.$getDataForEncrypt(e, i), t, r)
                );
            }),
            (n.prototype.getKeySize = function() {
                return this.keyPair.keySize;
            }),
            (n.prototype.getMaxMessageSize = function() {
                return this.keyPair.maxMessageLength;
            }),
            (n.prototype.$getDataForEncrypt = function(e, t) {
                if (_.isString(e) || _.isNumber(e))
                    return Buffer.from("" + e, t || "utf8");
                if (Buffer.isBuffer(e)) return e;
                if (_.isObject(e)) return Buffer.from(JSON.stringify(e));
                throw Error("Unexpected data type");
            }),
            (n.prototype.$getDecryptedData = function(e, t) {
                return "buffer" == (t = t || "buffer") ?
                    e :
                    "json" == t ?
                    JSON.parse(e.toString()) :
                    e.toString(t);
            }),
            n
        );
    })());
//# sourceMappingURL=/sm/18ea6bf652230f5ce3e77aa6f720c9a9b6274876713d694d60c6ca18ce20dc3b.map