const isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
// console.log("isSafari == ", isSafari);
var dropcount = 0;
var gamerun = 1;
var gametype = 1;
var time = 20;
var score = 00;
var countrun = 1;
var howto = 1;
var bubble = 1;
var new_bubble = 0;
var current_mouse_pos = 1;
var audio = $("#bgm")[0];
var audio2 = $("#correct_sound")[0];
var audio3 = $("#wrong_sound")[0];
var audio4 = $("#end_sound")[0];
var ranNums_1;

var current_page = 'home';

var total_point = 0;
var total_bomb = 0;
var total_point_item = 0;
var total_click = 0;

var stampTicket_Out_try = 0;

var TICKET = false;
var OTTK_BEFORE = "";
var OTTK_GAME = "";
var UTK = "";
var DISPLAY_NAME = "";
// const API_URL = "http://localhost:3001/ktb";
const API_URL = "https://api-test.krungthai-gamification.com/ktb";

const TOKEN = new URLSearchParams(window.location.search).get("token");

var FLAG_CONSENT = "unchecked"; // "checked" , "unchecked"
var FLAG_QUESTION = "unsubmit"; // "submit" , "unsubmit"

window.dataLayer = window.dataLayer || [];

function* shuffle_1(array) {
    var i = array.length;

    while (i--) {
        yield array.splice(Math.floor(Math.random() * (i + 1)), 1)[0];
    }
}

var ranNums_1 = shuffle_1([
    1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22,
    23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41,
    42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58,
]);

function goHome() {
    current_page = "home";
    location.reload();
}

function exit_game() {
    // console.log("EXIT");
    window.close();
}

function shareTo(target) {
    if (current_page == "score") {

    } else {
        if (target == "fb") {
            FB.ui({
                    display: "popup",
                    method: "feed",
                    link: "https://uat.krungthai-gamification.com",
                    ref: "gamepage",
                    hashtag: "#KTBHappyBirthdayGame",
                    quote: "มาเล่นเกมกันเถอะ, มีของรางวัลมากมาย",
                },
                function(response) {
                    // console.log("shered", response);
                }
            );
        }
    }

}

function hasToken() {
    if (!TOKEN || typeof TOKEN === "undefined") {
        Swal.fire({
            title: "ยินดีต้อนรับ",
            html: "กรุณาเข้าใช้งานผ่าน LINE Krungthai Connext (@krungthaiconnext)",
            icon: "info",
            confirmButtonText: "แอดไลน์",
            confirmButtonColor: "#19a7e4",
            padding: "1em",
            allowOutsideClick: false,
            allowEscapeKey: false,
        }).then((result) => {
            window.location.replace(
                "https://line.me/R/ti/p/@krungthaiconnext?from=ktbgame1"
            );
        });
    } else {
        firstShakehand();
    }
}

function checkValidInput(test, str) {
    let regex = new RegExp(test);
    let match = regex.test(str);
    // console.log(match, regex, str);
    if (str.length == 0) {
        return { case: 1, status: false };
    } else if (match == false) {
        return { case: 2, status: false };
    } else {
        return { case: 0, status: true };
    }
}

function registerGame() {
    // ============ STEP : 7 ============
    var json_data = {
        user_token: UTK,
        token: OTTK_GAME,
        // user_token: "TEST",
        // token: "TEST",
        product_name: $("#select_product").val(),
        display_name: encodeURIComponent($("#display_name").val()),
        first_name: $("#firstname").val(),
        last_name: $("#lastname").val(),
        telno: $("#phone").val(),
        email: $("#email").val(),
        address: $("#address").val(),
        province: $("#province").val(),
        district: $("#amphure").val(),
        sub_district: $("#tombon").val(),
        zipcode: $("#zipcode").val(),
    };

    var checklist = {
        display_name: checkValidInput(/.+/, json_data.display_name),
        first_name: checkValidInput(/[A-zก-์]{2,}/, json_data.first_name),
        last_name: checkValidInput(/[A-zก-์]{2,}/, json_data.last_name),
        telno: checkValidInput(
            /(^0[23457][\d]{7}$|^0[689][\d]{8}$)/,
            json_data.telno
        ),
        email: checkValidInput(/[^@\s]+@[^@\s]+\.[^@\s]+/, json_data.email),
        address: checkValidInput(/.{2,}/, json_data.address),
        province: checkValidInput(/.+/, json_data.province),
        district: checkValidInput(/.+/, json_data.district),
        sub_district: checkValidInput(/.+/, json_data.sub_district),
        zipcode: checkValidInput(/[\d]{5}/, json_data.zipcode),
    };

    // console.log(checklist);
    // check case
    if (checklist.display_name.status == false) {
        let html = "";
        let testcase = checklist.display_name.case;

        if (testcase == 1) {
            html = "ข้อมูลจำเป็นต้องกรอก,<br>กรุณากรอกข้อมูลให้ครบถ้วน";
        } else if (testcase == 2) {
            html =
                "รูปแบบการกรอกข้อมูลไม่ถูกต้อง,<br>กรุณากรอกด้วยตัวหนังสือภาษาไทยหรืออังกฤษ ไม่มีช่องว่าง, โปรดระวังข้อความหยาบคาย";
        }

        Swal.fire({
            title: "ข้อมูลไม่ถูกต้อง",
            html: "<u>[ ชื่อที่ใช้แสดงในอันดับ ]</u><br>" + html,
            icon: "warning",
            confirmButtonText: "กลับไปแก้ไข",
            confirmButtonColor: "#19a7e4",
            padding: "1em",
        });
        return false;
    } else if (checklist.first_name.status == false) {
        let html = "";
        let testcase = checklist.first_name.case;

        if (testcase == 1) {
            html = "ข้อมูลจำเป็นต้องกรอก,<br>กรุณากรอกข้อมูลให้ครบถ้วน";
        } else if (testcase == 2) {
            html =
                "รูปแบบการกรอกข้อมูลไม่ถูกต้อง, กรุณากรอกชื่อจริงข้อมูลด้วยภาษาไทยหรือภาษาอังกฤษ และไม่มีช่องว่างหรืออักขระพิเศษ";
        }

        Swal.fire({
            title: "กรุณาตรวจสอบข้อมูล",
            html: "<u>[ ชื่อจริง ]</u><br>" + html,
            icon: "warning",
            confirmButtonText: "กลับไปแก้ไข",
            confirmButtonColor: "#19a7e4",
            padding: "1em",
        });
        return false;
    } else if (checklist.last_name.status == false) {
        let html = "";
        let testcase = checklist.last_name.case;

        if (testcase == 1) {
            html = "ข้อมูลจำเป็นต้องกรอก,<br>กรุณากรอกข้อมูลให้ครบถ้วน";
        } else if (testcase == 2) {
            html =
                "รูปแบบการกรอกข้อมูลไม่ถูกต้อง, กรุณากรอกข้อมูลนามสกุลด้วยภาษาไทยหรือภาษาอังกฤษ และไม่มีช่องว่างหรืออักขระพิเศษ";
        }
        Swal.fire({
            title: "กรุณาตรวจสอบข้อมูล",
            html: "<u>[ นามสกุล ]</u><br>" + html,
            icon: "warning",
            confirmButtonText: "กลับไปแก้ไข",
            confirmButtonColor: "#19a7e4",
            padding: "1em",
        });
        return false;
    } else if (checklist.telno.status == false) {
        let html = "";
        let testcase = checklist.telno.case;

        if (testcase == 1) {
            html = "ข้อมูลจำเป็นต้องกรอก,<br>กรุณากรอกข้อมูลให้ครบถ้วน";
        } else if (testcase == 2) {
            html =
                "รูปแบบการกรอกข้อมูลไม่ถูกต้อง, กรุณากรอกเป็นตัวเลขทั้งหมดติดกัน ไม่มีช่องว่าง และต้องขึ้นต้นด้วย 0";
        }

        Swal.fire({
            title: "กรุณาตรวจสอบข้อมูล",
            html: "<u>[ เบอร์โทรศัพท์ ]</u><br>" + html,
            icon: "warning",
            confirmButtonText: "กลับไปแก้ไข",
            confirmButtonColor: "#19a7e4",
            padding: "1em",
        });
        return false;
    } else if (checklist.email.status == false) {
        let html = "";
        let testcase = checklist.email.case;

        if (testcase == 1) {
            html = "ข้อมูลจำเป็นต้องกรอก,<br>กรุณากรอกข้อมูลให้ครบถ้วน";
        } else if (testcase == 2) {
            html = "รูปแบบการกรอกข้อมูลไม่ถูกต้อง, กรุณากรอกอีเมลล์ให้ถูกต้อง";
        }

        Swal.fire({
            title: "กรุณาตรวจสอบข้อมูล",
            html: "<u>[ อีเมลล์ ]</u><br>" + html,
            icon: "warning",
            confirmButtonText: "กลับไปแก้ไข",
            confirmButtonColor: "#19a7e4",
            padding: "1em",
        });
        return false;
    } else if (checklist.address.status == false) {
        let html = "";
        let testcase = checklist.address.case;

        if (testcase == 1) {
            html = "ข้อมูลจำเป็นต้องกรอก,<br>กรุณากรอกข้อมูลให้ครบถ้วน";
        } else if (testcase == 2) {
            html =
                "รูปแบบการกรอกข้อมูลไม่ถูกต้อง, กรุณากรอกที่อยู่เลขที่บ้าน ชื่ออาคาร ซอย ถนน หรือหมู่บ้าน";
        }

        Swal.fire({
            title: "กรุณาตรวจสอบข้อมูล",
            html: "<u>[ ที่อยู่ ]</u><br>" + html,
            icon: "warning",
            confirmButtonText: "กลับไปแก้ไข",
            confirmButtonColor: "#19a7e4",
            padding: "1em",
        });
        return false;
    } else if (checklist.province.status == false) {
        let html = "";
        let testcase = checklist.province.case;

        if (testcase == 1) {
            html = "ข้อมูลจำเป็นต้องกรอก,<br>กรุณาเลือกจังหวัดจากในตัวเลือก";
        } else if (testcase == 2) {
            html = "รูปแบบการกรอกข้อมูลไม่ถูกต้อง";
        }

        Swal.fire({
            title: "กรุณาตรวจสอบข้อมูล",
            html: "<u>[ จังหวัด ]</u><br>" + html,
            icon: "warning",
            confirmButtonText: "กลับไปแก้ไข",
            confirmButtonColor: "#19a7e4",
            padding: "1em",
        });
        return false;
    } else if (checklist.district.status == false) {
        let html = "";
        let testcase = checklist.district.case;

        if (testcase == 1) {
            html =
                "ข้อมูลจำเป็นต้องกรอก,<br>กรุณาเลือก อำเภอ/เขต จากในตัวเลือก, โดยต้องทำการเลือกจังหวัดก่อน";
        } else if (testcase == 2) {
            html = "รูปแบบการกรอกข้อมูลไม่ถูกต้อง";
        }

        Swal.fire({
            title: "กรุณาตรวจสอบข้อมูล",
            html: "<u>[ อำเภอ/เขต ]</u><br>" + html,
            icon: "warning",
            confirmButtonText: "กลับไปแก้ไข",
            confirmButtonColor: "#19a7e4",
            padding: "1em",
        });
        return false;
    } else if (checklist.sub_district.status == false) {
        let html = "";
        let testcase = checklist.sub_district.case;

        if (testcase == 1) {
            html =
                "ข้อมูลจำเป็นต้องกรอก,<br>กรุณาเลือก ตำบล/แขวง จากในตัวเลือก, โดยต้องทำการเลือก จังหวัด และ อำเภอ/เขต ก่อนตามลำดับ";
        } else if (testcase == 2) {
            html = "รูปแบบการกรอกข้อมูลไม่ถูกต้อง";
        }

        Swal.fire({
            title: "กรุณาตรวจสอบข้อมูล",
            html: "<u>[ ตำบล/แขวง ]</u><br>" + html,
            icon: "warning",
            confirmButtonText: "กลับไปแก้ไข",
            confirmButtonColor: "#19a7e4",
            padding: "1em",
        });
        return false;
    } else if (checklist.zipcode.status == false) {
        let html = "";
        let testcase = checklist.zipcode.case;

        if (testcase == 1) {
            html =
                "ข้อมูลจำเป็นต้องกรอก,<br>รหัสไปรษณีย์จะกรอกอัตโนมัติ หากทำการเลือก จังหวัด, อำเภอ/เขต และ ตำบล/แขวง ตามลำดับ, สามารถแก้ไขได้";
        } else if (testcase == 2) {
            html = "รูปแบบการกรอกข้อมูลไม่ถูกต้อง";
        }

        Swal.fire({
            title: "กรุณาตรวจสอบข้อมูล",
            html: "<u>[ ตำบล/แขวง ]</u><br>" + html,
            icon: "warning",
            confirmButtonText: "กลับไปแก้ไข",
            confirmButtonColor: "#19a7e4",
            padding: "1em",
        });
        return false;
    }

    Swal.fire({
        title: "การยืนยัน",
        html: "ยืนยันข้อมูลที่ต้องการส่งหรือไม่?",
        icon: "info",
        confirmButtonText: "ยืนยัน",
        confirmButtonColor: "#19a7e4",
        showCancelButton: true,
        cancelButtonText: "แก้ไข",
        cancelButtonColor: "#999",
        reverseButtons: true,
        padding: "1em",
        didOpen: function() {
            // console.log("open confirm box");
            $(".swal2-confirm").attr("id", "button_confirm");
            $(".swal2-cancel").attr("id", "button_edit");
        },
    }).then(async(result) => {
        if (result.isConfirmed) {
            // console.log(json_data);
            let ct = await msg_encrypt(json_data);
            let key = ct.key;

            $.ajax({
                type: "POST",
                url: API_URL + "/game/submitQuestionnaire",
                data: JSON.stringify({
                    a: ct.a,
                    b: ct.b,
                    c: ct.c,
                }),
                dataType: "json",
                contentType: "application/json",
                success: async function(response) {
                    // console.log(response);
                    de_response = await msg_decrypt(key, response.b, response.c);
                    // console.log(de_response);
                    /* console.log(
                        "registerGame",
                        de_response.responseCode,
                        de_response.messageResponse
                    ); */
                    if (de_response.responseCode != 0) {
                        Swal.fire({
                            title: "ไม่สามารถบันทึกได้",
                            html: "กรุณาตรวจสอบความถูกต้องของข้อมูล",
                            icon: "error",
                            confirmButtonText: "ตกลง",
                            confirmButtonColor: "#19a7e4",
                            padding: "1em",
                        });
                        return false;
                    } else {
                        Swal.fire({
                            title: "บันทึกข้อมูลเรียบร้อย",
                            html: "ขอบคุณที่ร่วมทำแบบสอบถาม",
                            icon: "success",
                            confirmButtonText: "ตกลง",
                            confirmButtonColor: "#19a7e4",
                            padding: "1em",
                        }).then(() => {
                            change_step(8);
                        });
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    // console.log("Error: registerGame");
                    Swal.fire({
                        title: "ไม่สามารถบันทึกได้",
                        html: "กรุณาลองส่งใหม่อีกครั้ง",
                        icon: "error",
                        confirmButtonText: "ตกลง",
                        confirmButtonColor: "#19a7e4",
                        padding: "1em",
                    });
                    return false;
                },
            });
        } else {
            return false;
        }
    });
}

async function firstShakehand() {
    // ============ STEP : 1 ============
    var json_data = {};
    var reuse_user_token = sessionStorage.getItem("UTK");
    let ct = {};

    if (reuse_user_token == null) {
        json_data = { token: TOKEN };
        ct = await msg_encrypt(json_data);
    } else {
        ct = JSON.parse(reuse_user_token);
        // console.log("this user has sessionStorage", ct);
    }
    let key = ct.key;

    $.ajax({
        type: "POST",
        url: API_URL + "/auth/tokenBeforeGame",
        data: JSON.stringify({
            a: ct.a,
            b: ct.b,
            c: ct.c,
        }),
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        success: async function(response) {
            // console.log(response);
            de_response = await msg_decrypt(key, response.b, response.c);
            /* console.log(
                "firstShakehand",
                de_response.responseCode,
                de_response.messageResponse
            ); */

            if (de_response.responseCode != 0) {
                Swal.fire({
                    title: "ไม่สามารถดำเนินการได้",
                    html: "กรุณาเข้าใช้งานผ่าน LINE Krungthai Connext",
                    icon: "error",
                    confirmButtonText: "ปิดหน้าต่าง",
                    confirmButtonColor: "#19a7e4",
                    padding: "1em",
                }).then((result) => {
                    exit_game();
                });
            } else {
                OTTK_BEFORE = de_response.token;
                UTK = de_response.user_token;
                DISPLAY_NAME = decodeURIComponent(de_response.display_name);
                FLAG_CONSENT = de_response.consent_flag; // "checked" , "unchecked"
                FLAG_QUESTION = de_response.questionnaire_flag; // "submit" , "unsubmit"

                // console.log("Welcome = ", DISPLAY_NAME);
                // console.log("FLAG", FLAG_CONSENT, FLAG_QUESTION);

                window.dataLayer.push({
                    user_token: UTK,
                });

                $("#display_name").val(DISPLAY_NAME);
                EN_UTK = await msg_encrypt({ token: "", user_token: UTK });
                sessionStorage.setItem("UTK", JSON.stringify(EN_UTK));
                $(".loader").hide(400);
                setTimeout(() => {
                    $(".home-button").removeClass("d-none");
                    $(".home-button").addClass("d-flex");
                }, 400);
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            // console.log("Error: firstShakehand");
            Swal.fire({
                title: "ไม่สามารถดำเนินการได้",
                html: "กรุณาเข้าใช้งานผ่าน LINE Krungthai Connext",
                icon: "error",
                confirmButtonText: "ปิดหน้าต่าง",
                confirmButtonColor: "#19a7e4",
                padding: "1em",
            }).then((result) => {
                exit_game();
            });
        },
    });
}

function checkConsent() {
    if (FLAG_CONSENT == "checked") {
        change_step(2);
    } else {
        $("#terms_modal").modal("show");
    }
}

async function checkTicket() {
    // ============ STEP : 2 ============
    if (OTTK_BEFORE && OTTK_BEFORE.length) {
        var json_data = {
            user_token: UTK,
            token: OTTK_BEFORE,
        };
        let ct = await msg_encrypt(json_data);
        let key = ct.key;

        $.ajax({
            type: "POST",
            url: API_URL + "/auth/tokenChecktime/",
            data: JSON.stringify({
                a: ct.a,
                b: ct.b,
                c: ct.c,
            }),
            dataType: "json",
            contentType: "application/json",
            success: async function(response) {
                // onetimetoken
                de_response = await msg_decrypt(key, response.b, response.c);
                /* console.log(
                    "checkTicket",
                    de_response.responseCode,
                    de_response.messageResponse
                ); */
                //2001 = maximum
                if (de_response.responseCode == "2001") {
                    Swal.fire({
                        title: "คุณเล่นเกมเกินจำนวนที่กำหนด",
                        html: " กรุณาเข้าใช้งานอีกครั้งในวันถัดไป",
                        icon: "info",
                        confirmButtonText: "ตกลง",
                        confirmButtonColor: "#19a7e4",
                        padding: "1em",
                    }).then((result) => {
                        exit_game();
                    });
                } else if (de_response.responseCode != 0) {
                    Swal.fire({
                        title: "ไม่สามารถดำเนินการได้",
                        html: "กรุณาเข้าใช้งานผ่าน LINE Krungthai Connext",
                        icon: "error",
                        confirmButtonText: "ปิดหน้าต่าง",
                        confirmButtonColor: "#19a7e4",
                        padding: "1em",
                    }).then((result) => {
                        exit_game();
                    });
                } else {
                    buyTicket();
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                // console.log("Error: checkTicket");
                Swal.fire({
                    title: "ไม่สามารถดำเนินการได้",
                    html: "กรุณาเข้าใช้งานผ่าน LINE Krungthai Connext",
                    icon: "error",
                    confirmButtonText: "ปิดหน้าต่าง",
                    confirmButtonColor: "#19a7e4",
                    padding: "1em",
                }).then((result) => {
                    exit_game();
                });
            },
        });
    } else {
        // console.log("Error: checkTicket - NO TOKEN");
        Swal.fire({
            title: "ไม่สามารถดำเนินการได้",
            html: "กรุณาเข้าใช้งานผ่าน LINE Krungthai Connext",
            icon: "error",
            confirmButtonText: "ปิดหน้าต่าง",
            confirmButtonColor: "#19a7e4",
            padding: "1em",
        }).then((result) => {
            exit_game();
        });
    }
}

async function buyTicket() {
    // ============ STEP : 3 ============
    var json_data = { user_token: UTK };
    let ct = await msg_encrypt(json_data);
    let key = ct.key;

    $.ajax({
        type: "POST",
        url: API_URL + "/auth/getOnetimetoken",
        data: JSON.stringify({
            a: ct.a,
            b: ct.b,
            c: ct.c,
        }),
        dataType: "json",
        contentType: "application/json",
        success: async function(response) {
            de_response = await msg_decrypt(key, response.b, response.c);
            OTTK_GAME = de_response.api_token;
            /* console.log(
                "buyTicket",
                de_response.responseCode,
                de_response.messageResponse
            ); */
            if (de_response.responseCode != 0) {
                Swal.fire({
                    title: "ไม่สามารถดำเนินการได้",
                    html: "กรุณาเข้าใช้งานผ่าน LINE Krungthai Connext",
                    icon: "error",
                    confirmButtonText: "ปิดหน้าต่าง",
                    confirmButtonColor: "#19a7e4",
                    padding: "1em",
                }).then((result) => {
                    exit_game();
                });
            } else {
                // console.log("Game Starting...");
                TICKET = true;
                // ============= GAME-RUN  =============
                audio.volume = 0;
                audio.play();
                audio.currentTime = 0;
                $("#bgm").animate({ volume: 0.6 }, 1050);
                $(".choose-container").removeClass("active");
                setTimeout(function() {
                    $(".section").removeClass("d-block");
                }, 400);
                setTimeout(function() {
                    $(".section-game").addClass("d-block");
                    game_run();
                }, 1000);
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            // console.log("Error: buyTicket");
            Swal.fire({
                title: "ไม่สามารถดำเนินการได้",
                html: "กรุณาเข้าใช้งานผ่าน LINE Krungthai Connext",
                icon: "error",
                confirmButtonText: "ปิดหน้าต่าง",
                confirmButtonColor: "#19a7e4",
                padding: "1em",
            }).then((result) => {
                exit_game();
            });
        },
    });
}

async function stampTicket_In() {
    // ============ STEP : 04 ============
    var json_data = {
        user_token: UTK,
        api_token: OTTK_GAME,
        game_state: "start",
        consent_flag: "checked",
    };
    let ct = await msg_encrypt(json_data);
    let key = ct.key;

    $.ajax({
        type: "POST",
        url: API_URL + "/game/game_start/",
        data: JSON.stringify({
            a: ct.a,
            b: ct.b,
            c: ct.c,
        }),
        dataType: "json",
        contentType: "application/json",
        success: async function(response) {
            de_response = await msg_decrypt(key, response.b, response.c);
            /* console.log(
                "stampTicket_In",
                de_response.responseCode,
                de_response.messageResponse
            ); */

        },
        error: function(jqXHR, textStatus, errorThrown) {
            // console.log("Error: stampTicketIn");
            Swal.fire({
                title: "ไม่สามารถดำเนินการได้",
                html: "กรุณาเข้าใช้งานผ่าน LINE Krungthai Connext",
                icon: "error",
                confirmButtonText: "ปิดหน้าต่าง",
                confirmButtonColor: "#19a7e4",
                padding: "1em",
            }).then((result) => {
                exit_game();
            });
        },
    });
}

async function bigBrotherReporting(data) {
    // ============ STEP : 05 ============
    var json_data = {
        user_token: UTK,
        api_token: OTTK_GAME,
        state_flag: data.counter,
        total_point: data.total_point,
        total_bomb: data.total_bomb,
        total_click: data.total_click,
        score: data.score,
    };

    let ct = await msg_encrypt(json_data);
    let key = ct.key;

    $.ajax({
        type: "POST",
        url: API_URL + "/game/transectionsync",
        data: JSON.stringify({
            a: ct.a,
            b: ct.b,
            c: ct.c,
        }),
        dataType: "json",
        contentType: "application/json",
        success: async function(response) {
            de_response = await msg_decrypt(key, response.b, response.c);
            /* console.log(
                "bigBrotherReporting  : " + data.counter,
                de_response.responseCode,
                de_response.messageResponse
            ); */
        },
        error: function(jqXHR, textStatus, errorThrown) {
            // console.log("Error: bigBrotherisWatching - counter : " + data.counter);
            Swal.fire({
                title: "ไม่สามารถดำเนินการได้",
                html: "กรุณาลองเล่นใหม่อีกครั้ง",
                icon: "error",
                confirmButtonText: "ปิดหน้าต่าง",
                confirmButtonColor: "#19a7e4",
                padding: "1em",
            }).then((result) => {
                exit_game();
            });
        },
    });
}

function bigBrotherisWatching() {
    var counter = 0;
    // ============ STEP : 05 ============
    var watching = setInterval(function() {
        counter++;
        // console.log("Transactions watching : " + counter);

        if (counter == 4) {
            clearInterval(watching);
        } else {
            bigBrotherReporting({
                counter: counter,
                total_point: total_point,
                total_bomb: total_bomb,
                total_click: total_click,
                score: score,
            });
        }
    }, 5000);
}

async function stampTicket_Out() {
    // ============ STEP : 06 ============
    stampTicket_Out_try++;
    var json_data = {
        user_token: UTK,
        api_token: OTTK_GAME,
        total_point: total_point,
        total_bomb: total_bomb,
        total_click: total_click,
        consent_flag: FLAG_CONSENT,
        score: score,
    };
    let ct = await msg_encrypt(json_data);
    let key = ct.key;

    $.ajax({
        type: "POST",
        url: API_URL + "/game/submitendgame/",
        data: JSON.stringify({
            a: ct.a,
            b: ct.b,
            c: ct.c,
        }),
        dataType: "json",
        contentType: "application/json",
        success: async function(response) {
            de_response = await msg_decrypt(key, response.b, response.c);
            /* console.log(
                "stampTicket_Out",
                de_response.responseCode,
                de_response.messageResponse
            ); */
        },
        error: function(jqXHR, textStatus, errorThrown) {
            // console.log("Error: stampTicketOut");

            if (stampTicket_Out_try > 3) {
                Swal.fire({
                    title: "ไม่สามารถดำเนินการได้",
                    text: "กรุณาลองเข้าใหม่อีกครั้ง",
                    icon: "error",
                    confirmButtonText: "ปิดเกม",
                    confirmButtonColor: "#19a7e4",
                    padding: "1em",
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                }).then(() => {
                    exit_game();
                });
            } else {
                Swal.fire({
                    title: "ไม่สามารถดำเนินการได้",
                    html: "กำลังลองติดต่อ Server ใหม่อีกครั้ง<br>ใน <b>5</b> วินาที",
                    icon: "error",
                    showConfirmButton: false,
                    padding: "1em",
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    timer: 3000,
                    timerProgressBar: true,
                    didOpen: () => {
                        Swal.showLoading();
                        const b = Swal.getHtmlContainer().querySelector("b");
                        timerInterval = setInterval(() => {
                            b.textContent = Math.round(Swal.getTimerLeft() / 1000);
                        }, 1000);
                    },
                    willClose: () => {
                        clearInterval(timerInterval);
                    },
                }).then((result) => {
                    /* Read more about handling dismissals below */
                    if (result.dismiss === Swal.DismissReason.timer) {
                        stampTicket_Out();
                    }
                });
            }
        },
    });
}

async function viewHighscore() {
    $(".loader").show();
    var json_data = {
        user_token: UTK,
    };

    function censorship(name) {
        name = name.trim();
        const sara_top = [
            `็`,
            `ิ`,
            `ี`,
            `ุ`,
            `ู`,
            `ึ`,
            `ื`,
            `่`,
            `้`,
            `๊`,
            `๋`,
            `์`,
        ];

        var arr_name = Array.from(name);
        const index1 = arr_name.indexOf(`\ufe0f`);
        const index2 = arr_name.indexOf(`\ufe0e`);

        if (index1 > -1) {
            arr_name.splice(index1, 1);
        }
        if (index2 > -1) {
            arr_name.splice(index2, 1);
        }
        let lastIndex = arr_name.length - 1;
        let secondlastIndex = arr_name.length - 2;

        if (name.length >= 2 && sara_top.indexOf(arr_name[lastIndex]) > -1) {
            if (
                name.length >= 3 &&
                sara_top.indexOf(arr_name[secondlastIndex]) > -1
            ) {
                arr_name[secondlastIndex - 1] =
                    arr_name[secondlastIndex - 1] +
                    arr_name[secondlastIndex] +
                    arr_name[lastIndex];
                arr_name.splice(secondlastIndex, 2);
            } else {
                arr_name[lastIndex - 1] = arr_name[lastIndex - 1] + arr_name[lastIndex];
                arr_name.splice(lastIndex, 1);
            }
        }
        var arr_filter = arr_name.map((el) => {
            return encodeURIComponent(el);
        });

        return (
            // decodeURIComponent(arr_filter[0]) + "*".repeat(arr_name.length - 2) + decodeURIComponent(arr_filter.slice(-1))
            decodeURIComponent(arr_filter[0]) +
            " … " +
            decodeURIComponent(arr_filter.slice(-1))
        );
    }

    let ct = await msg_encrypt(json_data);
    let key = ct.key;
    $.ajax({
        type: "POST",
        url: API_URL + "/game/Getleaderboard/",
        data: JSON.stringify({
            a: ct.a,
            b: ct.b,
            c: ct.c,
        }),
        dataType: "json",
        contentType: "application/json",
        success: async function(response) {
            de_response = await msg_decrypt(key, response.b, response.c);
            /* console.log(
                "viewHighscore",
                de_response.responseCode,
                de_response.messageResponse
            ); */
            let list = de_response.data;
            let order_me = de_response.dataUser;

            list.forEach((element) => {
                let display_name_change = decodeURIComponent(
                    element.display_name_change
                );
                let display_name = decodeURIComponent(element.display_name);
                let name =
                    display_name_change &&
                    display_name_change.length &&
                    display_name_change != "null" ?
                    display_name_change.trim() :
                    display_name.trim();
                let num = element.order;
                let sumscore = element.total_score
                    .toString()
                    .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                var pattern_num = "0" + num;
                var num_format = pattern_num.substr(pattern_num.length - 2);

                var cen_name = censorship(name);
                $(".leader-row .no-" + num_format).addClass("active");
                $(".leader-row .no-" + num_format + " .leader-name").text(cen_name);
                $(".leader-row .no-" + num_format + " .leader-score score").text(
                    sumscore
                );
                if (order_me.order == num) {
                    $(".leader-row .no-" + num_format).addClass("my-score");
                }
            });
            if (order_me && order_me.order > 12) {
                var my_name = order_me.display_name_change ?
                    order_me.display_name_change.trim() :
                    order_me.display_name.trim();
                var cen_my_name = censorship(my_name);
                var myscore_html =
                    `<div class="no-` +
                    order_me +
                    ` my-score leader-row-item leader-row-bg2 mt-1"><a class="row-text2"><span>12.<span class="leader-name">-` +
                    cen_my_name +
                    `</span></span><span class="leader-score">คะแนนสะสม <score>` +
                    order_me.total_score +
                    `</score> คะแนน</span></a></div>`;
                $(".leader-row").append(myscore_html);
            }

            $(".loader").hide(400);
        },
        error: function(jqXHR, textStatus, errorThrown) {
            $(".loader").hide(400);
            // console.log("Error: viewHighscore");
            Swal.fire({
                title: "ไม่สามารถดึงข้อมูลคะแนนได้",
                text: "กรุณาลองเข้าใหม่ภายหลัง",
                icon: "error",
                confirmButtonText: "ตกลง",
                confirmButtonColor: "#19a7e4",
                padding: "1em",
            });
        },
    });
}

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split("&"),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split("=");

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ?
                true :
                decodeURIComponent(sParameterName[1]);
        }
    }
};

var fbID = getUrlParameter("fbID");
$("#profileFB").attr(
    "src",
    "https://graph.facebook.com/" + fbID + "/picture?type=large"
);

function change_step(x) {
    if (x == 2) {
        $("#terms_modal").modal("hide");
        $(".section").removeClass("d-block");
        $(".section-howto").addClass("d-block");
        setTimeout(function() {
            $(".howto-1").removeClass("howto-hidden");
            $(".howto-1").addClass("howto-show");
            $(".howto-3").removeClass("howto-hidden");
            $(".howto-nav").removeClass("howto-hidden");
            $(".howto-nav").addClass("howto-show");
        }, 600);
    }
    if (x == 3) {
        if (howto == 1) {
            $(".howto-1").removeClass("howto-show");
            $("#how-div").addClass("d-none");
            $("#how-back").addClass("d-none");
            $("#how-img").toggleClass("d-block d-none");
            $("#how-img_2").toggleClass("d-block d-none");
            $("#how-game").addClass("d-block");
            setTimeout(function() {
                $(".howto-1").addClass("howto-show");
            }, 10);
            howto = 2;
        } else if (howto == 2) {
            change_step(5);
        } else if (howto == 3) {
            change_step(5);
        }
    }
    if (x == 4) {
        $(".howto-3").addClass("howto-hidden");
        $(".howto-3").removeClass("howto-show");
        $(".howto-nav").addClass("howto-hidden");
        $(".howto-nav").removeClass("howto-show");
        setTimeout(function() {
            $(".section").removeClass("d-block");
        }, 400);
        setTimeout(function() {
            $(".section-choose").addClass("d-block");
            $(".choose-container").addClass("active");
        }, 500);
    }
    if (x == 5) {
        checkTicket();
        // then ==> buyTicket();
    }
    if (x == 6) {
        stampTicket_Out();
        // ============= GAME-FINISH =============
        $("#bgm").animate({ volume: 0 }, 900);
        audio.pause();
        if (FLAG_QUESTION == "submit") {
            $(".no_register").addClass("d-none");
            $(".registered").toggleClass("d-block d-none");
        }
        setTimeout(function() {
            $(".section").removeClass("d-block");
        }, 400);
        setTimeout(function() {
            $(".section-score").addClass("d-block");
            $(".score-container").addClass("active");
        }, 600);
    }
    if (x == 7) {
        setTimeout(function() {
            $(".section").removeClass("d-block");
        }, 400);
        setTimeout(function() {
            $(".section-register").addClass("d-block");
            $(".register-container").addClass("active");
        }, 600);
    }
    if (x == 8) {
        viewHighscore();
        setTimeout(function() {
            $(".section").removeClass("d-block");
        }, 400);
        setTimeout(function() {
            $(".section-board").addClass("d-block");
            $(".register-container").addClass("active");
        }, 600);
    }
    if (x == 9) {

        audio2.volume = 0;
        audio3.volume = 0;
        audio2.pause();
        audio3.pause();
        audio2.currentTime = 0;
        if (!isSafari) { audio2.play(); }
        $("#correct_sound").animate({ volume: 1 });
    }
    if (x == 10) {

        audio2.volume = 0;
        audio3.volume = 0;
        audio2.pause();
        audio3.pause();
        audio3.currentTime = 0;
        if (!isSafari) { audio3.play(); }
        $("#wrong_sound").animate({ volume: 1 });
    }
}

document.addEventListener(
    "touchmove",
    function(e) {
        // e.preventDefault();
        var mouse_position =
            ((e.touches[0].pageX - $("#game-container").offset().left) * 100) /
            $("#game-container").width();

        mouse_position_cal = (mouse_position * 80) / 100;
        if (mouse_position > 90) {
            mouse_position = 90;
        } else if (mouse_position > 10 && mouse_position <= 20) {
            $(".check-item").removeClass("active");
            $(".check1").addClass("active");
            current_mouse_pos = 1;
        } else if (mouse_position > 20 && mouse_position <= 40) {
            $(".check-item").removeClass("active");
            $(".check2").addClass("active");
            current_mouse_pos = 2;
        } else if (mouse_position > 40 && mouse_position <= 60) {
            $(".check-item").removeClass("active");
            $(".check3").addClass("active");
            current_mouse_pos = 3;
        } else if (mouse_position > 60 && mouse_position <= 80) {
            $(".check-item").removeClass("active");
            $(".check4").addClass("active");
            current_mouse_pos = 4;
        } else if (mouse_position > 80 && mouse_position <= 90) {
            $(".check-item").removeClass("active");
            $(".check5").addClass("active");
            current_mouse_pos = 5;
        } else if (mouse_position < 10) {
            mouse_position = 10;
        }
        $(".mouse-position").text("pageX: " + mouse_position);

        $(".bottle").css("margin-left", mouse_position + "%");
    },
    false
);

$(document).on("mousemove", function(event) {
    var mouse_position =
        ((event.pageX - $("#game-container").offset().left) * 100) /
        $("#game-container").width();

    mouse_position_cal = (mouse_position * 80) / 100;
    if (mouse_position > 90) {
        mouse_position = 90;
    } else if (mouse_position > 10 && mouse_position <= 20) {
        $(".check-item").removeClass("active");
        $(".check1").addClass("active");
        current_mouse_pos = 1;
    } else if (mouse_position > 20 && mouse_position <= 40) {
        $(".check-item").removeClass("active");
        $(".check2").addClass("active");
        current_mouse_pos = 2;
    } else if (mouse_position > 40 && mouse_position <= 60) {
        $(".check-item").removeClass("active");
        $(".check3").addClass("active");
        current_mouse_pos = 3;
    } else if (mouse_position > 60 && mouse_position <= 80) {
        $(".check-item").removeClass("active");
        $(".check4").addClass("active");
        current_mouse_pos = 4;
    } else if (mouse_position > 80 && mouse_position <= 90) {
        $(".check-item").removeClass("active");
        $(".check5").addClass("active");
        current_mouse_pos = 5;
    } else if (mouse_position < 10) {
        mouse_position = 10;
    }
    $(".mouse-position").text("pageX: " + mouse_position);

    $(".bottle").css("margin-left", mouse_position + "%");
});

function game_run() {
    time = 20;
    interval_loop();
    check_loop();
    countdown_run();
    stampTicket_In();
    bigBrotherisWatching();
}

function countdown_run() {
    gamerun = 1;
    setTimeout(function() {
        time = time - 1;
        if (time < 10) {
            $(".time-display").html("00 : 0" + time);
        } else {
            $(".time-display").html("00 : " + time);
        }

        if (time >= 1) {
            countdown_run();
        } else if (time <= 0) {
            gamerun = 0;
            setTimeout(function() {
                countrun = 0;
                game_finished();
            }, 3000);
        }
    }, 1000);
}

function interval_loop() {
    if (gamerun == 1) {
        var rand = Math.round(Math.random() * (1000 - 500)) + 400;
        setTimeout(function() {
            additem_to_row();
            interval_loop();
        }, rand);
    }
}

function additem_to_row() {
    var row_number = Math.floor(Math.random() * 5) + 1;
    var type = Math.floor(Math.random() * 2) + 1;
    row_number = row_number.toString();
    if (type == 1) {
        var icon = ranNums_1.next().value;
        $(".droprow-" + row_number).append(
            '<div dropcount="' +
            dropcount +
            '" type="' +
            type +
            '" class="icon-' +
            icon +
            " drop-item drop-item-" +
            dropcount +
            ' t-240"></div>'
        );
    } else {
        $(".droprow-" + row_number).append(
            '<div dropcount="' +
            dropcount +
            '" type="' +
            type +
            '" class="type-' +
            type +
            " drop-item drop-item-" +
            dropcount +
            ' t-240"></div>'
        );
    }
    setTimeout(function() {
        $(".drop-item-" + dropcount).css("margin-top", "calc(100vh + 120px)");
        $(".drop-item-" + (dropcount - 5)).remove();
        dropcount = dropcount + 1;
    }, 10);
}

let prevPOS = "";

function check_loop() {
    if (countrun == 1) {
        setTimeout(function() {
            $(".drop-item").each(function() {
                if ($(this).offset().top >= $(".bottom-check").offset().top - 80) {
                    $(this).addClass("bombed");
                    $(this).fadeOut(600, function() {
                        $(this).remove();
                    });
                }
            });

            if (current_mouse_pos != prevPOS) {
                total_click++;
            }
            prevPOS = current_mouse_pos;

            var current_drop = $(".droprow-" + current_mouse_pos + " .drop-item");
            if ($(current_drop).length) {
                offsetItem = $(current_drop).offset().top;
                offsetBottom = $(".bottom-check").offset().top;

                if (
                    offsetItem + 50 >= offsetBottom &&
                    offsetItem - 80 < offsetBottom &&
                    $(current_drop).hasClass("touched") == false
                ) {
                    var this_drop_item = $(current_drop).first();

                    if ($(current_drop).attr("type") == gametype) {
                        change_step(9);

                        total_point++;
                        score++;
                        $(".score-display2").html(score);
                        $(".score-display").html(score);

                        $(this_drop_item).addClass("touched winked");
                        $(".bottle-img").addClass("active");

                        setTimeout(function() {
                            $(".bottle-img").removeClass("active");
                            $(this_drop_item).remove();
                        }, 200);
                    } else if ($(current_drop).attr("type") != gametype) {
                        change_step(10);

                        total_bomb++;
                        if (score >= 1) {
                            score--;
                            $(".score-display2").html(score);
                            $(".score-display").html(score);
                        }

                        $(this_drop_item).addClass("touched exploded");
                        $(".bottle-img").addClass("bombed");

                        setTimeout(function() {
                            $(".bottle-img").removeClass("bombed");
                            $(this_drop_item).remove();
                        }, 200);
                    }
                }
            }
            check_loop();
        }, 1);
    }
}

function game_finished() {
    change_step(6);
    $(".bubble-item").removeClass("active");
    save_game();
}

function save_game() {
    $(".share-icon").attr(
        "href",
        "http://www.facebook.com/sharer/sharer.php?u=https://dev.uppercuz.com/ktb_game_betas/share.php?fbID=" +
        fbID
    );
}
$("#submit-btn").click(function() {
    if ($("#reg-form")[0].checkValidity() == false) {
        // alert('ข้อมูลที่กรอกไม่ถูกต้อง กรุณาลองอีกครั้ง');
    } else {
        event.preventDefault();
        setTimeout(function() {
            register();
        }, 1000);
    }
});

function register() {
    $("#reg-form").html(
        '<h2 style="margin-top:100px;color:#f75800;">ลงทะเบียนสำเร็จ</h2>'
    );

}

$(window).on("load", function() {
    $(".preloader").fadeOut("slow");
});

// ================================ LOADED ================================
$(document).ready(function() {
    // console.log("Ready");
    hasToken();

    $.fn.preload = function() {
        this.each(function() {
            $("<img/>")[0].src = this;
        });
    };
    // Usage:
    $([
        "assets/images/game_icon/g1.png",
        "assets/images/game_icon/g2.png",
        "assets/images/game_icon/g3.png",
        "assets/images/game_icon/g4.png",
        "assets/images/game_icon/g5.png",
        "assets/images/game_icon/g6.png",
        "assets/images/game_icon/g7.png",
        "assets/images/game_icon/g8.png",
        "assets/images/game_icon/g9.png",
        "assets/images/game_icon/g10.png",
        "assets/images/game_icon/g11.png",
        "assets/images/game_icon/g12.png",
        "assets/images/game_icon/g13.png",
        "assets/images/game_icon/g14.png",
        "assets/images/game_icon/g15.png",
        "assets/images/game_icon/g16.png",
        "assets/images/game_icon/g17.png",
        "assets/images/game_icon/g18.png",
        "assets/images/game_icon/g19.png",
        "assets/images/game_icon/g20.png",
        "assets/images/game_icon/g21.png",
        "assets/images/game_icon/g22.png",
        "assets/images/game_icon/g23.png",
        "assets/images/game_icon/g24.png",
        "assets/images/game_icon/g25.png",
        "assets/images/game_icon/g26.png",
        "assets/images/game_icon/g27.png",
        "assets/images/game_icon/g28.png",
        "assets/images/game_icon/g29.png",
    ]).preload();
    $("body").height(100);

    $(".section-video .howto-start").addClass("active");

    $(".w-viewport").html($(document).width());
    $(".h-viewport").html($(document).height());
    if (
        $(document).width() <= 576 &&
        $(document).width() / $(document).height() <= 0.6
    ) {
        $(".section-video .content-container .after").css(
            "padding-top",
            $(document).height() - 50
        ) + "px";
        $(".section-howto .content-container .after").css(
            "padding-top",
            $(document).height() - 50
        ) + "px";
        $(".section-choose .content-container .after").css(
            "padding-top",
            $(document).height() - 50
        ) + "px";
        $(".section-game .content-container .after").css(
            "padding-top",
            $(document).height() - 0
        ) + "px";
        $(".section-score .content-container .after").css(
            "padding-top",
            $(document).height() - 50
        ) + "px";
        $(".section-register .content-container .after").css(
            "padding-top",
            $(document).height() - 80
        ) + "px";
    } else if (
        $(document).width() <= 576 &&
        $(document).width() / $(document).height() > 0.6
    ) {
        $(".section-video .content-container .after").css(
            "padding-top",
            $(document).height() - 10
        ) + "px";
        $(".section-howto .content-container .after").css(
            "padding-top",
            $(document).height() - 10
        ) + "px";
        $(".section-choose .content-container .after").css(
            "padding-top",
            $(document).height() - 10
        ) + "px";
        $(".section-game .content-container .after").css(
            "padding-top",
            $(document).height() - 0
        ) + "px";
        $(".section-score .content-container .after").css(
            "padding-top",
            $(document).height() - 10
        ) + "px";
        $(".section-register .content-container .after").css(
            "padding-top",
            $(document).height() - 140
        ) + "px";
    }
});

$(window).scroll(function(d, h) {
    z = $(window).scrollTop();
    if (z >= $("#section-fastest").offset().top - $(window).height() + 250) {
        $(".nextg")
            .delay(300)
            .queue(function() {
                $(".nextg").addClass("active");
                $(".fibre").addClass("active");
                $(this).dequeue();
            });
    }
});