var L5J44czD9D1u7140t0p1C01E5QPd7COf = new JSEncrypt();
L5J44czD9D1u7140t0p1C01E5QPd7COf.setPublicKey(`-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAkTIrziLabSXxmVf0G/ey
Cd2HMxZS9CsQE2qgmxHb4JyzOR5A2W8WrLM0nva/5B1WcBgSGG3lqiVnm68EMU4L
Q9By4mL+nD6nJTrPUCasDSH/kPcWQ7atuILAuMofySufSwxjA5K2iIzO9eufCGJ1
+MVERqIeW2Bl33uXvmmVVFFNdrr1uBiS3IbZKm2bFX5A9rPMTs3Slyrdp0ekdmR3
UfOqsRrglrnEfRK0ASlYBPyXhT8rQbLlSZcnl2TGSKaBAiBL0kSYVDVTclwcmPH9
QFxugoa7yNQaY9CYJMLGvW/AUy4ouDWWyx3yL9Dtqb4BLnbN9jvJH6wm14ja7X36
ZQIDAQAB
-----END PUBLIC KEY-----`);


function buf2hex(buffer) {
    // buffer is an ArrayBuffer
    return [...new Uint8Array(buffer)].map(x => x.toString(16).padStart(2, '0')).join('');
}

const ORZe67 = async(pt) => {
    pt['t'] = new Date().getTime() / 1000;
    const key = secureRandom(24);
    const ptBytes = aesjs.utils.utf8.toBytes(JSON.stringify(pt));

    const aesCTR = new aesjs.ModeOfOperation.ctr(key);

    const encKey = L5J44czD9D1u7140t0p1C01E5QPd7COf.encrypt(buf2hex(key));
    const encMsg = aesCTR.encrypt(ptBytes);

    const mac = CryptoJS.HmacSHA256(
        CryptoJS.enc.Hex.parse(buf2hex(encMsg)),
        CryptoJS.enc.Hex.parse(buf2hex(key))
    );

    const macHex = CryptoJS.enc.Hex.stringify(mac);

    return {
        key: key,
        a: encKey,
        b: buf2hex(encMsg),
        c: macHex,
    };
}

const V9P09B = async(key, ct, mac_param) => {

    const ctBytes = aesjs.utils.hex.toBytes(ct);

    const mac = CryptoJS.HmacSHA256(
        CryptoJS.enc.Hex.parse(buf2hex(ctBytes)),
        CryptoJS.enc.Hex.parse(buf2hex(key))
    );
    const macHex = CryptoJS.enc.Hex.stringify(mac);

    if (mac_param !== macHex) throw Error("Manipulated ?");

    const aesCTR = new aesjs.ModeOfOperation.ctr(key);
    const dataBytes = aesCTR.decrypt(ctBytes)
    const data = aesjs.utils.utf8.fromBytes(dataBytes);

    const now = new Date().getTime() / 1000;
    if (now - data.t > 30) throw Error("Hack ?");

    return JSON.parse(data);
}


// (async() => {
// let ct = await ORZe67("Hello world");
//     let pt = await V9P09B(ct.key, ct.b, ct.c);
//     console.log(pt);
// })().then();