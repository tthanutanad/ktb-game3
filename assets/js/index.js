$(window).on("load", function() {
    // $(".content-panel-inner").mCustomScrollbar();
    $(".product-item").matchHeight();
    var url = "assets/province_amphure.json";
    var tombon_url = "assets/thai_tombons.json";
    fetch(url, {
            method: "GET", // or 'PUT'
            headers: {
                "Content-Type": "application/json",
            },
        })
        .then((response) => response.json())
        .then((data) => {
            // console.log("Success - 1");
            var tombon_data = $.ajax({
                    method: "GET",
                    url: tombon_url,
                    async: false,
                })
                .done(function(data) {
                    // console.log("Success - 2");
                    // tombon_data = data
                })
                .fail(function() {
                    // console.log("Erroe - 2");
                });

            let tombon_json = tombon_data.responseJSON.RECORDS;
            // console.log(tombon_json);

            data.map((item) => {
                // console.log("i", item);
                $("#province").append(
                    '<option value="' +
                    item.name_th +
                    '" data-id="' +
                    item.id +
                    '">' +
                    item.name_th +
                    "</option>"
                );
            });
            $("#province").change(function(e) {
                e.preventDefault();
                let id = $(this).find(":selected").data("id");
                var prov = data.filter((item) => {
                    return item.id == id;
                });
                $("#zipcode").val("");
                $("#amphure").html('<option value="" selected>เลือกอำเภอ/เขต</option>');
                $("#tombon").html('<option value="" selected>เลือกตำบล/แขวง</option>');
                prov[0].amphure.forEach(item => {
                    $("#amphure").append(
                        '<option value="' +
                        item.name_th +
                        '" data-id="' +
                        item.id +
                        '">' +
                        item.name_th +
                        "</option>"
                    );
                });
            });

            $("#amphure").change(function(e) {
                e.preventDefault();
                let pid = $("#province").find(":selected").data("id");
                let id = $(this).find(":selected").data("id");
                let pattern = new RegExp(`^${id}`);
                // console.log(pattern);

                var tombon = tombon_json.filter((item) => {
                    // if (pattern.test(item.id)) {
                    //     console.log('filter', item);
                    // }
                    return pattern.test(item.id);
                });
                $("#zipcode").val('');
                $("#tombon").html('<option value="" selected>เลือกตำบล/แขวง</option>');

                // console.log("tombon", tombon);
                tombon.forEach(item => {
                    // console.log("item", item);
                    $("#tombon").append(
                        '<option value="' +
                        item.name_th +
                        '" data-zipcode="' +
                        item.zip_code +
                        '">' +
                        item.name_th +
                        "</option>"
                    );
                });

            });

            $("#tombon").change(function(e) {
                e.preventDefault();
                if ($(this).val() != "") {
                    let zipcode = $(this).find(":selected").data("zipcode");
                    // console.log(zipcode);
                    $("#zipcode").val(zipcode);
                }
            });

        })
        .catch((error) => {
            // console.error("Error:", error);
        });
});

$(".table-bg").scroll(function() {
    $(this).find(".scroll-hand").css("opacity", "0");
});