# How to Integrate Code
### Overview Flow
https://docs.google.com/spreadsheets/d/1JWfPIYBhK05P8k26VN9tz_9VzPy99EqgCYri29PkQ7k/edit#gid=0

### File that you can edit
1. Main File
```
./assets/js/game3/readme.js 
```
- This files have 3 type of function
- Type 1. Render game
- - goLoading, goHome, goGame
- Type 2. Request data form API
- - getConsentInfo, sendScore, checkUserPermissionToPlay, syncDataWhenEndStage, getRanks
- Type 3. Share and Popup Policy
- -  openPolicyPopupWithNoConsent, openPolicyPopupFromHome, shareScore, shareFriends
- Read more detail in Overflow Flow (google sheet)

2. Form Submit Information
```
./assets/js/game3/submit-form/existing-form.js 
```
- registerGame() will be called when user sumbit information function
- Information has 6 fields which refer to div id
- #select_product #display_name #firstname #lastname #phone #email #province
